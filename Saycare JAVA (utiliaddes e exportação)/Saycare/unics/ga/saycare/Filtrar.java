package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;

public class Filtrar
{
	public static void main(String[] args) throws IOException
	{
		filtrar("Dados.txt");
	}

	private static void filtrar(String filepath) throws IOException
	{
		File file = new File(filepath);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		List<Integer> ids = new DynamicList<>();

		System.out.println(br.readLine());

		while (br.ready())
		{
			String line = br.readLine();
			int idRead = IntUtil.parse(line.split("\t")[0]);
			boolean contains = false;

			for (int id : ids)
				if (idRead == id)
				{
					contains = true;
					break;
				}

			if (contains)
			{
				System.out.println(line);
				ids.add(idRead);
			}
		}

		br.close();
	}
}
