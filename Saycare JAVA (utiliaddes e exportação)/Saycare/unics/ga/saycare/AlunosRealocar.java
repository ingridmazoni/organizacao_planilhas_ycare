package ga.saycare;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.Queue;
import org.diverproject.util.collection.abstraction.DynamicQueue;

import tk.saycare.Conexao;

public class AlunosRealocar
{
	public static void main(String[] args) throws SQLException
	{
		Connection connectionOriginal = Conexao.getConnection(false);
		Connection connectionRelease = Conexao.getConnection(true);
		Queue<Aluno> alunos = carregarAlunos(connectionOriginal);

		System.out.printf("Encontrado %d alunos.\n", alunos.size());

		String sql = "REPLACE INTO alunos (id, center, age_group, school) VALUES (?, ?, ?, ?)";

		for (Aluno aluno : alunos)
		{
			PreparedStatement ps = connectionRelease.prepareStatement(sql);
			ps.setInt(1, aluno.id);
			ps.setInt(2, aluno.center);
			ps.setInt(3, aluno.ageGroup);
			ps.setInt(4, aluno.school);

			if (ps.executeUpdate() > 0)
				System.out.printf("Aluno %d adicionado com sucesso.\n", aluno.id);
		}

		connectionOriginal.close();
		connectionRelease.close();
	}

	private static Queue<Aluno> carregarAlunos(Connection connection) throws SQLException
	{
		Queue<Aluno> queue = new DynamicQueue<>();

		String sql = "SELECT alunos.id, alunos.faixa_etaria, escolas.tipo FROM alunos INNER JOIN escolas ON escolas.id = alunos.escola ORDER BY alunos.id";

		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		while (rs.next())
		{
			Aluno aluno = new Aluno();
			aluno.id = rs.getInt("id");
			aluno.center = Integer.parseInt(Character.toString(Integer.toString(aluno.id).charAt(0)));
			aluno.ageGroup = faixaEtaria(rs.getInt("faixa_etaria"));
			aluno.school = rs.getInt("tipo");
			queue.offer(aluno);
		}

		return queue;
	}

	private static int faixaEtaria(int faixa)
	{
		switch (faixa)
		{
			case 0: return 2;
			case 1: return 1;
			case 2: return 0;
		}

		return 0;
	}

	private static class Aluno
	{
		private int id;
		private int center;
		private int ageGroup;
		private int school;
	}
}
