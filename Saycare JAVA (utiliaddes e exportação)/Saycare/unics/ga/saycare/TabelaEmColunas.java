package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.StringMap;

public class TabelaEmColunas
{
	public static void main(String args[]) throws IOException
	{
		File file = new File("Dados.txt");
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		String colunas[] = br.readLine().split("\t");
		StringMap<List<String>> dados = new StringMap<>();

		for (int i = 0; i < colunas.length; i++)
			dados.add(colunas[i], new DynamicList<>());

		while (br.ready())
		{
			String valores[] = br.readLine().split("\t");

			for (int i = 1; i < valores.length; i++)
				dados.get(colunas[i]).add(String.format("%s\t%s", valores[0], valores[i]));
		}

		br.close();

		for (String coluna : colunas)
			for (String valor : dados.get(coluna))
				System.out.printf("%s\t%s\n", coluna, valor);
	}
}
