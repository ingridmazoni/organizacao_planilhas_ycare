package ga.saycare;

import static org.diverproject.util.MessageUtil.die;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;
import org.diverproject.util.sql.MySQL;

public class RelatorioContagem
{
	private static final MySQL sql = new MySQL();
	private static final List<Aluno> alunos = new DynamicList<>();

	static
	{
		sql.setHost("localhost");
		sql.setDatabase("saycare_release");
		sql.setUsername("saycare");
		sql.setPassword("saycare2015");

		try {
			sql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	public static void main(String[] args)
	{
		loadAlunos("Dados.txt");

		loadCity(new City(1, "S�o Paulo"));
		loadCity(new City(2, "Teresina"));
		loadCity(new City(3, "Buenos Aires"));
		loadCity(new City(4, "Medellin"));
		loadCity(new City(5, "Lima"));
		loadCity(new City(6, "Montevideo"));
		loadCity(new City(7, "Santiago"));
	}

	private static void loadAlunos(String filepath)
	{
		try{

			File file = new File(filepath);
			FileReader in = new FileReader(file);
			BufferedReader br = new BufferedReader(in);

			while (br.ready())
			{
				String line = br.readLine();
				String columns[] = line.split("\t");

				Aluno aluno = new Aluno();
				aluno.id = IntUtil.parse(columns[0]);
//				aluno.center = IntUtil.parse(columns[1]);
				aluno.age = IntUtil.parse(columns[2]);
//				aluno.school = IntUtil.parse(columns[3]);
				alunos.add(aluno);
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void loadCity(City city)
	{
		loadAge(0, city);
		loadAge(1, city);
		loadAge(2, city);

		printCity(city);
	}

	private static void loadAge(int age, City city)
	{
		for (Aluno aluno : alunos)
			if (aluno.age == age && IntUtil.interval(aluno.id, city.getMinAlunoID(), city.getMaxAlunoID()))
				city.count[age]++;
	}

	private static void printCity(City city)
	{
		System.out.printf("%16s: ", city.name);
		System.out.printf("%d\t", city.count[0]);
		System.out.printf("%d\t", city.count[1]);
		System.out.printf("%d\n", city.count[2]);
	}

	private static class City
	{
		private int center;
		private String name;
		private int count[] = new int[3];

		public City(int center, String name)
		{
			this.center = center;
			this.name = name;
		}

		public int getMinAlunoID()
		{
			return Integer.parseInt(String.format("%d000000", center));
		}

		public int getMaxAlunoID()
		{
			return Integer.parseInt(String.format("%d999999", center));
		}
	}

	private static class Aluno
	{
		private int id;
//		private int center;
		private int age;
//		private int school;
	}
}
