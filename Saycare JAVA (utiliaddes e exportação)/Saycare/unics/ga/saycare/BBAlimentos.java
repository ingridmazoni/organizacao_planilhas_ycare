package ga.saycare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.diverproject.util.DateUtil;
import org.diverproject.util.collection.List;
import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.IntegerAvarageMap;
import org.diverproject.util.collection.abstraction.StringSimpleMap;
import org.diverproject.util.lang.IntUtil;

public class BBAlimentos
{
	public static void main(String[] args) throws IOException
	{
		/*Map<String, String> labels = lerLabels("C://Users//FMUSP//Desktop//6.Dados_2017//dieta_2017//BBAlimentosLabels.txt");*/
		Map<Integer, Recordatorio> recordatorios = lerRecordatorios("C://Users//FMUSP//Desktop//6.Dados_2017//dieta_2017//BBAlimentos.txt");
		/*escreverRecordatorios(labels, recordatorios, "C://Users//FMUSP//Desktop//6.Dados_2017//dieta_2017//BBAlimentos Exportados.txt");*/
	}

	private static Map<String, String> lerLabels(String filepath) throws IOException
	{
		Map<String, String> labels = new StringSimpleMap<>();

		File file = new File(filepath);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		while (br.ready())
		{
			String linha = br.readLine().toLowerCase();
			String colunas[] = linha.split("\t");

			if (!labels.add(colunas[0], colunas[1]))
				System.out.println("alimento '" +colunas[0]+ "' n�o foi adicionado como '" +colunas[1]+ "'");
		}

		br.close();

		return labels;
	}

	private static Map<Integer, Recordatorio> lerRecordatorios(String filepath) throws IOException
	{
		Map<Integer, Recordatorio> recordatorios = new IntegerAvarageMap<>();

		File file = new File(filepath);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		for (int i = 1; br.ready(); i++)
		{
			String line = br.readLine();

			if (line.isEmpty())
				continue;

			String columns[] = line.split("\t");

			if (columns.length != 7)
			{
				System.out.printf("Linha %d tem %d colunas de %d.\n", i, columns.length, 8);
				continue;
			}

			try {

				int id = IntUtil.parseString(columns[0]);
				String iniciais = columns[1];
				Date data = columns[2].equals(".") ? null : DateUtil.toDate(columns[2]);

				if (data == null)
					continue;

				String nome = columns[4].toLowerCase();
				float quantidade = Float.parseFloat(columns[5].replace(',', '.'));

//				int tipo = IntUtil.parseString(columns[3]);
//				String unidade = columns[6];

				Alimento alimento = new Alimento();
				alimento.nome = nome;
				alimento.quantidade = quantidade;
//				alimento.tipo = tipo;
//				alimento.unidade = unidade;

				Recordatorio recordatorio = recordatorios.get(id);

				if (recordatorio == null)
				{
					recordatorio = new Recordatorio();
					recordatorio.id = id;
					recordatorio.iniciais = iniciais;
					recordatorios.add(id, recordatorio);
				}

				for (int j = 0; j < recordatorio.datas.length; j++)
				{
					if (recordatorio.datas[j] != null && !recordatorio.datas[j].equals(data))
						continue;

					if (recordatorio.datas[j] == null)
					{
						recordatorio.datas[j] = data;
						recordatorio.alimentos[j] = new StringSimpleMap<>();
					}

					StringSimpleMap<Alimento> alimentos = recordatorio.alimentos[j];

					if (alimentos.containsKey(nome))
						alimentos.get(nome).quantidade += quantidade;
					else
					{
						alimentos.add(nome, alimento);

						if (!Alimento.existentes.contains(nome))
							Alimento.existentes.add(nome);
					}

					break;
				}

			} catch (Exception e) {
				System.out.printf("Linha %d causou %s.\n", i, e.getMessage());
				continue;
			}
		}

		br.close();

		return recordatorios;
	}

	private static void escreverRecordatorios(Map<String, String> labels, Map<Integer, Recordatorio> recordatorios, String filepath) throws IOException
	{
		File file = new File(filepath);
		FileWriter writer = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(writer);

		bw.write("AlunoID\tIniciais\t");

		for (int i = 1; i <= 3; i++)
		{
			bw.write("Dia"+ i +"\t");

			for (String nome : Alimento.existentes)
			{
				String label = labels.get(nome);

				if (label == null)
				{
					System.out.println("alimento '" +nome+ "' n�o encontrado");
					bw.write(nome);
					bw.write("\t");
				}

				else
				{
					bw.write(label);
					bw.write("\t");
				}
			}
		}

		bw.newLine();

		for (Recordatorio recordatorio : recordatorios)
		{
			System.out.printf("Exportando %d (%s) ... ", recordatorio.id, recordatorio.iniciais);

			bw.write(Integer.toString(recordatorio.id));
			bw.write("\t");
			bw.write(recordatorio.iniciais);
			bw.write("\t");

			for (int i = 0; i < recordatorio.datas.length; i++)
			{
				bw.write(DateUtil.toString(recordatorio.datas[i], "dd/MM/YYYY"));
				bw.write("\t");

				StringSimpleMap<Alimento> alimentos = recordatorio.alimentos[i];

				for (String nome : Alimento.existentes)
				{
					float quantidade = 0.0f;

					if (alimentos != null)
					{
						Alimento alimento = alimentos.get(nome);
						quantidade += alimento == null ? 0.0f : alimento.quantidade;
					}

					bw.write(String.format("%.1f", quantidade));
					bw.write("\t");
				}
			}

			bw.newLine();

			System.out.println("OK!");
		}

		bw.close();
	}

	private static class Recordatorio
	{
		private int id;
		private String iniciais;
		private Date datas[];
		private StringSimpleMap<Alimento> alimentos[];

		@SuppressWarnings("unchecked")
		public Recordatorio()
		{
			datas = new Date[3];
			alimentos = new StringSimpleMap[3];
		}
	}

	private static class Alimento
	{
		private static final List<String> existentes = new DynamicList<>();

//		private int tipo;
		private String nome;
		private float quantidade;
//		private String unidade;

		public Alimento()
		{
			quantidade = 0.0f;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj != null && obj instanceof Alimento)
			{
				Alimento nome = (Alimento) obj;

				return nome.nome.equals(nome);
			}

			return false;
		}
	}
}
