package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.Map.MapItem;
import org.diverproject.util.collection.abstraction.StringSimpleMap;
import org.diverproject.util.lang.FloatUtil;

public class AgruparVariaveis
{
	public static void main(String[] args) throws IOException
	{
		File file = new File("AgruparVariaveis.txt");
		FileReader in = new FileReader(file);
		BufferedReader reader = new BufferedReader(in);

		int total = 0;
		StringSimpleMap<Integer> map = new StringSimpleMap<>();

		while (reader.ready())
		{
			String line = reader.readLine();

			if (line.isEmpty())
				continue;

			String columns[] = line.split("\t");

			String value = columns[1];

			if (map.containsKey(value))
				map.update(value, map.get(value) + 1);
			else
				map.add(value, 1);

			total++;
		}

		reader.close();

		for (MapItem<String, Integer> item : map.iterateItems())
			System.out.printf("%s: %d de %d (%.2f)\n", item.key, item.value, total, FloatUtil.toPorcent(item.value, total));
	}
}
