package ga.saycare;

import static org.diverproject.util.MessageUtil.die;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.UtilRuntimeException;
import org.diverproject.util.collection.abstraction.StringSimpleMap;
import org.diverproject.util.lang.IntUtil;
import org.diverproject.util.sql.MySQL;

@SuppressWarnings("unchecked")
public class TabelaDados
{
	private static final MySQL sql = new MySQL();
	private static final StringSimpleMap<Integer> labels[] = new StringSimpleMap[3];

	static
	{
		sql.setHost("localhost");
		sql.setDatabase("saycare_release");
		sql.setUsername("saycare");
		sql.setPassword("saycare2015");

		try {
			sql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	public static void main(String[] args) throws IOException
	{
		for (int i = 0; i < labels.length; i++)
		{
			labels[i] = new StringSimpleMap<>();
			loadLabels(i, labels[i]);
		}

		for (File file : new File("TabelaDados").listFiles())
			if (file.isFile())
				load(file);
	}

	private static void loadLabels(int age, StringSimpleMap<Integer> labels)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT id, name FROM labels WHERE age_group = ?";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, age);

			ResultSet rs = ps.executeQuery();

			while (rs.next())
				labels.add(rs.getString("name"), rs.getInt("id"));

			System.out.printf("Encontrado %d labels para age %d.\n", labels.size(), age);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void load(File file)
	{
		try {

			System.out.printf("Iniciando leitura de: %s\n", file.getAbsoluteFile());

			FileReader in = new FileReader(file);
			BufferedReader br = new BufferedReader(in);

			String columns[] = br.readLine().split("\t");

			while (br.ready())
			{
				String line = br.readLine();

				if (line.isEmpty())
					continue;

				String values[] = line.split("\t");

				insertAluno(values);
				insertData(columns, values);
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void insertAluno(String[] values)
	{
		int alunoID = IntUtil.parse(values[0]);
		int center = IntUtil.parse(values[1]);
		int age = IntUtil.parse(values[2]);
		int school = IntUtil.parse(values[3]);

		Connection connection = sql.getConnection();
		String sql = "INSERT INTO alunos (id, center, age_group, school) VALUES (?, ?, ?, ?)";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, alunoID);
			ps.setInt(2, center);
			ps.setInt(3, age);
			ps.setInt(4, school);

			if (ps.executeUpdate() == 1)
				System.out.printf("Aluno de ID %d inserido com �xito!\n", alunoID);

		} catch (SQLException e) {
			if (e.getErrorCode() != 1062)
				System.out.printf("Falha ao inserir Aluno de ID %d (%s).\n", alunoID, e.getMessage());
		}
	}

	private static void insertData(String[] columns, String[] values)
	{
		int alunoID = IntUtil.parse(values[0]);
		int age = IntUtil.parse(values[2]);

		Connection connection = sql.getConnection();
		String sql = "REPLACE INTO respostas (aluno, label, valor) VALUES ";

		if (columns.length != values.length)
		{
			System.out.printf("Aluno de ID %d com formato inv�lido!\n", alunoID);
			return;
		}

		for (int i = 4; i < values.length; i++)
		{
			String label = columns[i];
			Integer labelID = labels[age].get(label);

			if (labelID == null)
			{
				new UtilRuntimeException("%d (%d) n�o possui %s", alunoID, age, label).printStackTrace();;
				continue;
			}

			sql += "(?, ?, ?)";

			if (i < values.length - 1)
				sql += ",";
		}

		try {

			PreparedStatement ps = connection.prepareStatement(sql);

			for (int i = 4, j = 0; i < values.length; i++, j++)
			{
				String label = columns[i];
				Integer labelID = labels[age].get(label);

				if (labelID == null)
					throw new UtilRuntimeException("%d n�o possui %s", alunoID, label);

				ps.setInt((j * 3) + 1, alunoID);
				ps.setInt((j * 3) + 2, labelID);
				ps.setString((j * 3) + 3, values[i]);
			}

			System.out.printf("Alocado %d respostas para Aluno de ID %d.\n", ps.executeUpdate(), alunoID);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
