package ga.saycare;

import static org.diverproject.util.lang.IntUtil.parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;

public class VerificarIDsDiferentes
{
	public static void main(String[] args) throws IOException
	{
		List<Integer> originais = lerIDsOriginais("Dados.txt");
		List<Integer> exportados = lerIDsOriginais("Alunos.txt");

		for (int id : originais)
		{
			boolean found = false;

			for (int i = 0; i < exportados.size(); i++)
				if (exportados.get(i) == id)
				{
					found = true;
					break;
				}

			if (!found)
				;//System.out.printf("Aluno %d n�o foi exportado\n", id);
			else
				System.out.printf("Aluno %d foi exportado\n", id);
		}

		for (int id : exportados)
		{
			boolean found = false;

			for (int i = 0; i < originais.size(); i++)
				if (originais.get(i) == id)
				{
					found = true;
					break;
				}

			if (!found)
				;//System.out.printf("Aluno %d n�o existe\n", id);
			else
				;//System.out.printf("Aluno %d existe\n", id);
		}
	}

	private static List<Integer> lerIDsOriginais(String filename) throws IOException
	{
		File file = new File(filename);
		FileReader in = new FileReader(file);
		BufferedReader reader = new BufferedReader(in);
		List<Integer> ids = new DynamicList<>();

		while (reader.ready())
			ids.add(parse(reader.readLine()));

		reader.close();

		return ids;
	}
}
