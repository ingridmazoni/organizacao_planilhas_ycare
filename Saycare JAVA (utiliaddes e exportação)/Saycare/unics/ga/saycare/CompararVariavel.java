package ga.saycare;

import static org.diverproject.util.lang.IntUtil.parse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.IntegerLargeMap;

public class CompararVariavel
{
	private static List<Integer> ids = new DynamicList<>();

	public static void main(String args[]) throws IOException
	{
		IntegerLargeMap<String> variaveisAlteradas = ler("Dados.txt");
		IntegerLargeMap<String> variaveisReais = ler("Dados2.txt");

		transcrever(variaveisReais, variaveisAlteradas, "Dados3.txt");
	}

	private static IntegerLargeMap<String> ler(String filename) throws IOException
	{
		File file = new File(filename);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);
		IntegerLargeMap<String> variaveis = new IntegerLargeMap<>();

		br.readLine();
		{
			while (br.ready())
			{
				String line = br.readLine();
				String colunas[] = line.split("\t");
				String variavel = colunas[1];
				int alunoID = parse(colunas[0]);
				variaveis.add(alunoID, variavel);

				if (!ids.contains(alunoID))
					ids.add(alunoID);
			}
		}
		br.close();

		return variaveis;
	}

	private static void transcrever(IntegerLargeMap<String> variaveisReais, IntegerLargeMap<String> variaveisAlteradas, String filename) throws IOException
	{
		File file = new File(filename);
		FileWriter out = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(out);

		for (int i = 0; i < ids.size(); i++)
		{
			int alunoID = ids.get(i);

			if (!variaveisAlteradas.containsKey(alunoID))
				continue;

			bw.write(String.format("%d\t%s\t%s", alunoID, value(variaveisReais.get(alunoID)), value(variaveisAlteradas.get(alunoID))));
			bw.newLine();
		}

		bw.flush();
		bw.close();
	}

	private static Object value(String string)
	{
		return string == null ? "null" : string;
	}
}
