package ga.saycare;

import static org.diverproject.util.MessageUtil.die;
import static org.diverproject.util.Util.format;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.UtilRuntimeException;
import org.diverproject.util.collection.abstraction.StringSimpleMap;
import org.diverproject.util.lang.IntUtil;
import org.diverproject.util.sql.MySQL;

@SuppressWarnings("unchecked")
public class TabelaDados2
{
	private static final MySQL sql = new MySQL();
	private static final StringSimpleMap<Integer> labels[] = new StringSimpleMap[3];

	static
	{
		sql.setHost("localhost");
		sql.setDatabase("saycare_release");
		sql.setUsername("saycare");
		sql.setPassword("saycare2015");

		try {
			sql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	public static void main(String[] args) throws IOException
	{
		for (int i = 0; i < labels.length; i++)
		{
			labels[i] = new StringSimpleMap<>();
			loadLabels(i, labels[i]);
		}

		load(new File("Dados.txt"), 2);
		load(new File("Dados2.txt"), 1);
		load(new File("Dados3.txt"), 0);
	}

	private static void loadLabels(int age, StringSimpleMap<Integer> labels)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT id, name FROM labels WHERE age_group = ?";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, age);

			ResultSet rs = ps.executeQuery();

			while (rs.next())
				labels.add(rs.getString("name"), rs.getInt("id"));

			System.out.printf("Encontrado %d labels para age %d.\n", labels.size(), age);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void load(File file, int ageGroup)
	{
		try {

			FileReader in = new FileReader(file);
			BufferedReader br = new BufferedReader(in);

			String columns[] = br.readLine().split("\t");

			while (br.ready())
			{
				String line = br.readLine();

				if (line.isEmpty())
					continue;

				String values[] = line.split("\t");
				int alunoID = IntUtil.parse(values[0]);
				int ageGroupSetted = IntUtil.parse(values[2]);

				if (ageGroupSetted != ageGroup)
				{
					br.close();
					throw new UtilRuntimeException(format("aluno %d est� com age_group %d em %d", alunoID, ageGroupSetted, ageGroup));
				}

				insertAluno(values);
				insertData(columns, values);
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void insertAluno(String[] values)
	{
		int alunoID = IntUtil.parse(values[0]);
		int center = IntUtil.parse(values[1]);
		int age = IntUtil.parse(values[2]);
		int school = IntUtil.parse(values[3]);

		Connection connection = sql.getConnection();
		String sql = "INSERT INTO alunos (id, center, age_group, school) VALUES (?, ?, ?, ?)";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, alunoID);
			ps.setInt(2, center);
			ps.setInt(3, age);
			ps.setInt(4, school);

			if (ps.executeUpdate() == 1)
				System.out.printf("Aluno de ID %d inserido com �xito!\n", alunoID);

		} catch (SQLException e) {
			if (e.getErrorCode() != 1062)
				System.out.printf("Falha ao inserir Aluno de ID %d (%s).\n", alunoID, e.getMessage());
		}
	}

	private static void insertData(String[] columns, String[] values)
	{
		int alunoID = IntUtil.parse(values[0]);
		int age = IntUtil.parse(values[2]);

		if (columns.length != values.length)
		{
			System.out.printf("Aluno de ID %d com formato inv�lido!\n", alunoID);
			return;
		}

		Connection connection = sql.getConnection();
		String sql = "REPLACE INTO respostas (aluno, label, valor) VALUES (?, ?, ?)";

		try {

			int replaced = 0;

			for (int i = 4; i < values.length; i++)
			{
				PreparedStatement ps = connection.prepareStatement(sql);

				String label = columns[i];
				Integer labelID = labels[age].get(label);

				if (labelID == null)
					continue;//throw new UtilRuntimeException("%d n�o possui %s", alunoID, label);

				ps.setInt(1, alunoID);
				ps.setInt(2, labelID);
				ps.setString(3, values[i]);

				if (ps.executeUpdate() != 1)
					throw new UtilRuntimeException(format("falha em aluno: %d, label: %s e valor: %s", alunoID, label, values[i]));
				else
					replaced++;
			}

			System.out.printf("Alocado %d respostas para Aluno de ID %d.\n", replaced, alunoID);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
