package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;

import tk.saycare.Conexao;

public class Alunos
{
	public static void main(String[] args) throws IOException, SQLException
	{
		List<Integer> ids = identificar("Alunos.txt");
		List<Aluno> alunos = carregar(ids);

		imprimir(alunos);
	}

	private static List<Integer> identificar(String filePath) throws IOException
	{
		File file = new File(filePath);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);

		List<Integer> ids = new DynamicList<>();

		for (int i = 0; br.ready(); i++)
		{
			String line = br.readLine();

			if (i == 0)
				continue;

			if (IntUtil.isInteger(line))
				ids.add(Integer.parseInt(line));
		}

		br.close();

		return ids;
	}

	private static List<Aluno> carregar(List<Integer> ids) throws SQLException
	{
		Connection connection = Conexao.getConnection(false);

		List<Aluno> alunos = new DynamicList<>();
		String sql = "SELECT alunos.faixa_etaria as age_group, escolas.tipo as school FROM alunos "
					+"INNER JOIN escolas ON escolas.id = alunos.escola "
					+"WHERE alunos.id = ?";

		for (int id : ids)
		{
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			Aluno aluno = new Aluno();
			alunos.add(aluno);
			aluno.id = id;

			if (rs.next())
			{
				aluno.faixa = rs.getInt("age_group");
				aluno.escola = rs.getInt("school");
			}
		}

		return alunos;
	}

	private static void imprimir(List<Aluno> alunos) throws IOException
	{
		System.out.printf("ID_number\tcenter\tage_group1\tschool\n");

		for (Aluno aluno : alunos)
			System.out.printf("%d\t%s\t%s\t%s\n", aluno.id, Integer.toString(aluno.id).charAt(0), parseFaixa(aluno.faixa), parseEscola(aluno.escola));
	}

	private static String parseFaixa(int faixa)
	{
		switch (faixa)
		{
			case 0: return "2";
			case 1: return "1";
			case 2: return "0";
		}

		return null;
	}

	private static String parseEscola(int escola)
	{
		switch (escola)
		{
			case 1: return "0";
			case 2: return "1";
		}

		return "2";
	}

	private static class Aluno
	{
		private int id;
		private int faixa;
		private int escola;
	}
}
