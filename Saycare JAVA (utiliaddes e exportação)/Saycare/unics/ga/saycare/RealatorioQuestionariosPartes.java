package ga.saycare;

import static org.diverproject.util.MessageUtil.die;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.sql.MySQL;

public class RealatorioQuestionariosPartes
{
	private static final MySQL sql = new MySQL();

	static
	{
		sql.setHost("localhost");
		sql.setDatabase("saycare_release");
		sql.setUsername("saycare");
		sql.setPassword("saycare2015");

		try {
			sql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	public static void main(String[] args)
	{
		loadLabel("wat2");
		loadLabel("ffqcma_Q2");

		loadLabel("ac_seden_wd");
		loadLabel("sba1_Q1");
		loadLabel("ant5d");
		loadLabel("ant6d");
		loadLabel("ant10d");
		loadLabel("ant12d");
		loadLabel("ant13d");
		loadLabel("ant14d");
		loadLabel("ant15d");
		loadLabel("ant16d");
		loadLabel("ant17d");
		loadLabel("ant18d");

		loadLabels("Personal history", "hp1_Q1", "hp2_Q1", "hp3_Q1", "hp4_Q1", "hp5_Q1", "hp6_Q1", "hp7_Q1", "");
		loadLabels("Demographic data", "");
		loadLabels("Socioeconomic Status", "fs9_Q1", "fs10a_Q1", "fs10b_Q1", "fs10c_Q1", "fs10d_Q1", "fs10e_Q1", "fs10f_Q1", "fs10g_Q1", "fs10h_Q1", "fs11_Q1", "fs12_Q1", "fs13_Q1", "fs14_Q1", "fs15_Q1", "");
		loadLabels("Environmental Factors", "fa1_Q1", "fa2_Q1", "fa3_Q1", "fa4_Q1", "fa5_Q1", "fa6_Q1", "fa7_Q1", "fa8_Q1", "fa9_Q1", "fa10_Q1", "fa11_Q1", "fa12_Q1", "fa13_Q1", "fa14_Q1", "fa15_Q1");
		loadLabels("Physical Activty", "af331_Q1", "af341_Q1", "af351_Q1", "af7_Q1", "af8_Q1", "af9_Q1", "af10_Q1", "af11_Q1", "af12_Q1", "af13_Q1", "af14_Q1", "af15_Q1", "");
		loadLabels("Sedentary Behavior", "cs5_Q1", "cs6_Q1", "cs7_Q1", "cs8_Q1", "cs9_Q1", "cs10_Q1", "cs11_Q1", "cs12_Q1", "cs13_Q1", "cs14_Q1", "cs15_Q1", "cs16_Q1", "cs17_Q1", "cs18a_Q1", "cs18b_Q1");
		loadLabels("Sleep Time/Quality", "hs8_Q1", "hs9_Q1", "hs10_Q1", "hs11_Q1", "hs12_Q1", "hs13_Q1", "hs14a_Q1", "hs14b_Q1", "hs14c_Q1", "hs14d_Q1", "hs15a_Q1", "hs15b_Q1", "hs15c_Q1", "hs15d_Q1");
		loadLabels("Food Frequency Questionnaire");
		loadLabels("Dietary Patterns Determinants", "");
		loadLabels("Pubertal Stages", "ma1girls_Q1", "ma2girls_Q1", "ma3girls_Q1", "ma1boys_Q1", "ma2boys_Q1");
	}

	public static void loadLabel(String label)
	{
		System.out.println("\n -- " +label+" -- \n");

		loadCity(new City(1, "S�o Paulo"), label);
		loadCity(new City(2, "Teresina"), label);
		loadCity(new City(3, "Buenos Aires"), label);
		loadCity(new City(4, "Medellin"), label);
		loadCity(new City(5, "Lima"), label);
		loadCity(new City(6, "Montevideo"), label);
		loadCity(new City(7, "Santiago"), label);
	}

	private static void loadLabels(String name, String... labels)
	{
		System.out.println("\n -- " +name+" -- \n");

		loadCity(new City(1, "S�o Paulo"), labels);
		loadCity(new City(2, "Teresina"), labels);
		loadCity(new City(3, "Buenos Aires"), labels);
		loadCity(new City(4, "Medellin"), labels);
		loadCity(new City(5, "Lima"), labels);
		loadCity(new City(6, "Montevideo"), labels);
		loadCity(new City(7, "Santiago"), labels);
	}

	private static void loadCity(City city, String label)
	{
		loadAge(city, label, 0);
		loadAge(city, label, 1);
		loadAge(city, label, 2);

		printCity(city);
	}

	private static void loadCity(City city, String[] labels)
	{
		loadAge(city, labels, 0);
		loadAge(city, labels, 1);
		loadAge(city, labels, 2);

		printCity(city);
	}

	private static void loadAge(City city, String label, int age)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT COUNT(DISTINCT aluno) as size "
					+"FROM respostas "
					+"INNER JOIN alunos ON alunos.id = respostas.aluno "
					+"INNER JOIN labels ON labels.id = respostas.label "
					+"WHERE alunos.age_group = ? AND alunos.id >= ? AND alunos.id <= ? "
					+"AND labels.name = ? AND respostas.valor != '.'";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, age);
			ps.setInt(2, city.getMinAlunoID());
			ps.setInt(3, city.getMaxAlunoID());
			ps.setString(4, label);

			ResultSet rs = ps.executeQuery();

			if (rs.next())
				city.count[age] = rs.getInt("size");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void loadAge(City city, String[] labels, int age)
	{
		String labelSQL = "labels.name = ?";

		for (int i = 1; i < labels.length; i++)
			labelSQL += " OR labels.name = ?";

		Connection connection = sql.getConnection();
		String sql = "SELECT COUNT(DISTINCT aluno) as size "
					+"FROM respostas "
					+"INNER JOIN alunos ON alunos.id = respostas.aluno "
					+"INNER JOIN labels ON labels.id = respostas.label "
					+"WHERE alunos.age_group = ? AND alunos.id >= ? AND alunos.id <= ? "
					+"AND respostas.valor != '.' AND (" +labelSQL+ ")";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, age);
			ps.setInt(2, city.getMinAlunoID());
			ps.setInt(3, city.getMaxAlunoID());

			for (int i = 0; i < labels.length; i++)
				ps.setString(4 + i, labels[i]);

			ResultSet rs = ps.executeQuery();

			if (rs.next())
				city.count[age] = rs.getInt("size");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void printCity(City city)
	{
		for (int count : city.count)
			System.out.println(count);
	}

	private static class City
	{
		private int center;
		private int count[] = new int[3];

		public City(int center, String name)
		{
			this.center = center;
		}

		public int getMinAlunoID()
		{
			return Integer.parseInt(String.format("%d000000", center));
		}

		public int getMaxAlunoID()
		{
			return Integer.parseInt(String.format("%d999999", center));
		}
	}
}
