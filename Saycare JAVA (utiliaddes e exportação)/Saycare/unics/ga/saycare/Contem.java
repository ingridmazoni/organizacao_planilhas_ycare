package ga.saycare;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Contem
{
	public static void main(String[] args) throws IOException
	{
		String first[] = read("C:/Users/Andrew/Desktop/a.txt");
		String second[] = read("C:/Users/Andrew/Desktop/b.txt");

		for (String f : first)
		{
			boolean contains = false;

			for (String s : second)
				if (f.equals(s))
				{
					contains = true;
					break;
				}

			if (!contains)
				System.out.println(f);
		}
	}

	private static String[] read(String filepath) throws IOException
	{
		File file = new File(filepath);
		FileReader in = new FileReader(file);
		BufferedReader reader = new BufferedReader(in);

		String line = reader.readLine();
		String values[] = line.split(",");

		reader.close();

		return values;
	}
}
