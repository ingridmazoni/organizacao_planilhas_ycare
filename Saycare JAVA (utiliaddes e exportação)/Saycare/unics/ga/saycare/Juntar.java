package ga.saycare;

import static org.diverproject.util.MessageUtil.die;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.abstraction.IntegerAvarageMap;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;
import org.diverproject.util.collection.abstraction.StringMap;
import org.diverproject.util.sql.MySQL;

public class Juntar
{
	private static final MySQL sql = new MySQL();

	static
	{
		sql.setHost("localhost");
		sql.setDatabase("saycare_release");
		sql.setUsername("saycare");
		sql.setPassword("saycare2015");

		try {
			sql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException
	{
		IntegerAvarageMap<Aluno> alunos = new IntegerAvarageMap<>();
		StringMap<Label> labels[][] = new StringMap[3][14];
		IntegerLittleMap<Label> labelsEnum[][] = new IntegerLittleMap[3][14];

		for (int i = 0; i < labels.length; i++)
			for (int j = 0; j < labels[i].length; j++)
				labels[i][j] = new StringMap<>();

		for (int i = 0; i < labelsEnum.length; i++)
			for (int j = 0; j < labelsEnum[i].length; j++)
				labelsEnum[i][j] = new IntegerLittleMap<>();

		loadAlunos(alunos);
		loadLabels(labels, labelsEnum);
		alocate(labels, alunos);
		prepareWrite(labelsEnum, alunos);
	}

	private static void loadAlunos(IntegerAvarageMap<Aluno> alunos)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT id, center, age_group, school FROM alunos";// WHERE id >= 1000000 AND id <= 1019999

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next())
			{
				Aluno aluno = new Aluno();
				aluno.id = rs.getInt("id");
				aluno.center = rs.getInt("id") / 1000000;
				aluno.age = rs.getInt("age_group");
				aluno.school = rs.getInt("school");
				alunos.add(aluno.id, aluno);

				System.out.printf("Aluno#%d carregado.\n", aluno.id);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void loadLabels(StringMap<Label>[][] labels, IntegerLittleMap<Label>[][] labelsEnum)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT id, questionario, age_group, enum, name FROM labels ORDER BY id";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next())
			{
				Label label = new Label();
//				label.id = rs.getInt("id");
				label.qid = rs.getInt("questionario");
				label.num = rs.getInt("enum");
				label.name = rs.getString("name");
				labels[rs.getInt("age_group")][label.qid - 1].add(label.name, label);
				labelsEnum[rs.getInt("age_group")][label.qid - 1].add(label.num, label);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static void alocate(StringMap<Label>[][] labels, IntegerAvarageMap<Aluno> alunos)
	{
		for (Aluno aluno : alunos)
		{
			aluno.answers = new StringMap[14];

			for (int i = 0; i < aluno.answers.length; i++)
				aluno.answers[i] = new StringMap<>();

			for (int qid = 1; qid <= aluno.answers.length; qid++)
				loadAnswers(aluno, labels[aluno.age][qid - 1], aluno.answers[qid - 1], qid);
		}
	}

	private static void loadAnswers(Aluno aluno, StringMap<Label> labels, StringMap<LabelValue> answers, int qid)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT labels.name, valor FROM respostas"
					+" INNER JOIN labels ON labels.id = respostas.label"
					+" WHERE aluno = ? AND labels.questionario = ?"
					+" ORDER BY labels.enum";

		System.out.printf("Carregando respostas de Aluno#%d:qid=%2d... ", aluno.id, qid);

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, aluno.id);
			ps.setInt(2, qid);

			ResultSet rs = ps.executeQuery();

			while (rs.next())
			{
				LabelValue value = new LabelValue();
				value.label = labels.get(rs.getString("name"));
				value.answer = rs.getString("valor");

				answers.add(value.label.name, value);
			}

			System.out.printf(" %d lidos!.\n", answers.size());

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void prepareWrite(IntegerLittleMap<Label>[][] labels, IntegerAvarageMap<Aluno> alunos)
	{
		write("DadosEscolares.txt", labels, alunos, 0, 1);
		write("DadosAdolescentes.txt", labels, alunos, 2);
	}

	private static void write(String filepath, IntegerLittleMap<Label>[][] labels, IntegerAvarageMap<Aluno> alunos, int... ages)
	{
		try {

			File file = new File(filepath);
			FileWriter out = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(out);

			for (int age : ages)
			{
				writeHead(bw, labels[age]);

				for (Aluno aluno : alunos)
					if (aluno.age == age)
						writeOn(bw, aluno, labels[age]);
			}

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeHead(BufferedWriter bw, IntegerLittleMap<Label>[] labels) throws IOException
	{
		bw.write("ID_number	center	age_group1	school");

		for (IntegerLittleMap<Label> questionario : labels)
			for (Label label : questionario)
				bw.write("	" +label.name);

		bw.newLine();
		bw.flush();
	}

	private static void writeOn(BufferedWriter bw, Aluno aluno, IntegerLittleMap<Label>[] qLabels) throws IOException
	{
		System.out.printf("Escrevendo respostas de Aluno#%d... ", aluno.id);

		bw.write(String.format("%d	%d	%d	%d", aluno.id, aluno.center, aluno.age, aluno.school));

		for (int i = 0; i < qLabels.length; i++)
		{
			IntegerLittleMap<Label> labels = qLabels[i];

			for (Label label : labels)
			{
				LabelValue value = aluno.answers[i].get(label.name);

				if (value == null)
					bw.write("	.");
				else
					bw.write("	" +value.answer);
			}
		}

		System.out.println(" OK!");

		bw.newLine();
		bw.flush();
	}

	private static class Aluno
	{
		private int id;
		private int center;
		private int age;
		private int school;
		private StringMap<LabelValue> answers[];
	}

	private static class Label
	{
//		private int id;
		private int qid;
		private int num;
		private String name;
	}

	private static class LabelValue
	{
		private Label label;
		private String answer;
	}
}
