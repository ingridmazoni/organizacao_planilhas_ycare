package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FiltrarPontos
{
	public static void main(String[] args) throws IOException
	{
		filtrar("Dados.txt");
	}

	private static void filtrar(String filepath) throws IOException
	{
		File file = new File(filepath);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		while (br.ready())
		{
			String line = br.readLine();
			line = line.replace(".", "0");

			String values[] = line.split("\t");
			boolean has = false;

			for (int i = 0; i < values.length; i++)
				if (!values[i].equals("0"))
				{
					has = true;
					break;
				}

			if (!has)
				line = line.replaceAll("0", ".");

			System.out.println(line);
		}

		br.close();
	}
}
