package ga.saycare;

import static org.diverproject.util.Util.format;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.Map.MapItem;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;
import org.diverproject.util.collection.abstraction.StringSimpleMap;

public class NovaEsportacao
{
	private static Map<Integer, StringSimpleMap<String>> alunos;
	private static List<String> labels;

	public static void main(String args[]) throws IOException
	{
		processar("Dados.txt");
	}

	private static void processar(String filename) throws IOException
	{
		alunos = new IntegerLittleMap<>();
		labels = new DynamicList<>();

		lerQeustionarioTabela(filename);
		lerMudancas("Mudan�as.txt");
		escreverQuestionarioTabela(filename);
	}

	private static void lerQeustionarioTabela(String filename) throws IOException
	{
		File file = new File(filename);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		String colunas[] = br.readLine().split("\t");

		for (int i = 0; i < colunas.length; i++)
		{
			if (colunas[i] == null)
			{
				br.close();
				throw new IOException(format("coluna %d n�o definida", i + 1));
			}

			labels.add(colunas[i].trim());
		}

		for (int i = 2; br.ready(); i++)
		{
			String linha = br.readLine();
			String valores[] = linha.split("\t");

			if (valores.length != labels.size())
			{
				br.close();
				throw new IOException(format("linha com %d valores de %d (linha: %d)", valores.length, labels.size(), i));
			}

			int alunoID = Integer.parseInt(valores[0]);
			StringSimpleMap<String> respostas = new StringSimpleMap<>();
			{
				for (int j = 0; j < valores.length; j++)
				{
					String label = labels.get(j);
					respostas.add(label, valores[j].trim());
				}
			}

			if (alunos.add(alunoID, respostas))
				System.out.printf("Lido %d com %d respostas.\n", alunoID, respostas.size());
			else
			{
				br.close();
				throw new IOException(format("falha ao registrar respostas do aluno %d", alunoID));
			}
		}

		br.close();
	}

	private static void lerMudancas(String filename) throws IOException
	{
		File file = new File(filename);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		for (int i = 1; br.ready(); i++)
		{
			String linha = br.readLine();
			String valores[] = linha.split("\t");

			if (valores.length != 3)
			{
				br.close();
				throw new IOException(format("linha com %d valores de 3 (linha: %d)", valores.length, i));
			}

			int alunoID = Integer.parseInt(valores[1]);
			String label = valores[0];
			String resposta = valores[2];

			StringSimpleMap<String> respostas = alunos.get(alunoID);

			if (respostas == null)
				continue;

			if (respostas.update(label, resposta))
				System.out.printf("Atualizando %d.%s para '%s'\n", alunoID, label, resposta);

			else if (labels.contains(label))
			{
				br.close();
				throw new IOException(format("falha ao atualizar %d.%s", alunoID, label));
			}
		}

		br.close();
	}

	private static void escreverQuestionarioTabela(String filename) throws IOException
	{
		File file = new File("Gen" +filename);
		FileWriter writer = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(writer);

		for (MapItem<Integer, StringSimpleMap<String>> aluno : alunos.iterateItems())
		{
			for (int i = 0; i < labels.size(); i++)
			{
				String label = labels.get(i);
				String resposta = aluno.value.get(label);

				if (resposta == null)
				{
					bw.close();
					throw new IOException(format("reposta para '%s' n�o encontrado no aluno %d", label, aluno.key));
				}

				bw.write(resposta);

				if (i != labels.size() - 1)
					bw.write("\t");
			}

			/*
			if (!aluno.value.get("dp_sex_Q1").equals(".") &&
				!aluno.value.get("dp_birth_Q1").equals(".") &&
				!aluno.value.get("weight").equals(".") &&
				!aluno.value.get("height_cm").equals("."))
				bw.write("\t1");
			else
				bw.write("\t0");

			if (!aluno.value.get("dp_sex_Q2").equals(".") &&
				!aluno.value.get("dp_birth_Q2").equals(".") &&
				!aluno.value.get("weight").equals(".") &&
				!aluno.value.get("height_cm").equals("."))
				bw.write("\t1");
			else
				bw.write("\t0");
			*/

			bw.newLine();
			bw.flush();

			System.out.printf("Dados do aluno %d escritos.\n", aluno.key);
		}

		bw.close();
	}
}
