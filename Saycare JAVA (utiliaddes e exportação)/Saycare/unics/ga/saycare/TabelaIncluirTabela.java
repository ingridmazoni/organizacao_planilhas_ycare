package ga.saycare;

import static org.diverproject.util.Util.format;
import static org.diverproject.util.lang.IntUtil.parse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;
import org.diverproject.util.collection.abstraction.StringSimpleMap;

public class TabelaIncluirTabela
{
	private static List<Integer> ids;
	private static List<String> labels;
	private static IntegerLittleMap<StringSimpleMap<String>> dados;

	public static void main(String args[]) throws IOException
	{
		incluir("Escolares_PreEscolares.txt", "IncluirEscolares_PreEscolares.txt");
	}

	private static void incluir(String source, String additional) throws IOException
	{
		ids = new DynamicList<>();
		labels = new DynamicList<>();
		dados = new IntegerLittleMap<>();

		readSource(source);
		readAdditional(additional);
		writeData("Gen" +source);
	}

	private static void readSource(String filename) throws IOException
	{
		File file = new File("TabelaDados", filename);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		String colunas[] = br.readLine().split("\t");

		for (int i = 0; i < colunas.length; i++)
			labels.add(colunas[i]);

		while (br.ready())
		{
			String valores[] = br.readLine().split("\t");
			StringSimpleMap<String> alunoDados = new StringSimpleMap<>();
			int alunoID = parse(valores[0]);
			ids.add(alunoID);

			for (int i = 1; i < valores.length; i++)
				alunoDados.add(labels.get(i), valores[i]);

			dados.add(alunoID, alunoDados);

			System.out.printf("Dados do aluno %d lidos\n", alunoID);
		}

		br.close();
	}

	private static void readAdditional(String filename) throws IOException
	{
		File file = new File("TabelaDados", filename);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		String colunas[] = br.readLine().split("\t");

		for (int i = 1; i < colunas.length; i++)
			if (!labels.contains(colunas[i]))
				labels.add(colunas[i]);
			else
				colunas[i] = null;

		while (br.ready())
		{
			String valores[] = br.readLine().split("\t");
			int alunoID = parse(valores[0]);

			if (!ids.contains(alunoID))
				continue;

			StringSimpleMap<String> alunoDados = dados.get(alunoID);

			for (int i = 1; i < valores.length; i++)
			{
				if (colunas[i] == null)
					continue;

				if (!alunoDados.add(colunas[i], valores[i]))
					if (!alunoDados.update(colunas[i], valores[i]))
					{
						br.close();
						throw new RuntimeException(format("aluno %d n�o pode adicionar %s (%s)", alunoID, colunas[i], valores[i]));
					}
			}

			System.out.printf("Dados do aluno %d adicionados\n", alunoID);
		}

		br.close();
	}

	private static void writeData(String filename) throws IOException
	{
		File file = new File("TabelaDados", filename);
		FileWriter out = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(out);

		for (int i = 0; i < labels.size(); i++)
		{
			bw.write(labels.get(i));

			if (i < labels.size() - 1)
				bw.write("\t");
		}

		bw.newLine();

		for (int alunoID : ids)
		{
			StringSimpleMap<String> valores = dados.get(alunoID);

			bw.write(Integer.toString(alunoID));

			for (int i = 1; i < labels.size(); i++)
			{
				String valor = valores.get(labels.get(i));

				bw.write("\t");
				bw.write(valor == null ? "."  : valor.isEmpty() ? "." : valor);
			}

			bw.newLine();
			bw.flush();

			System.out.printf("Dados do aluno %d escritos.\n", alunoID);
		}

		bw.close();
	}
}
