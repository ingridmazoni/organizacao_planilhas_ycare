package ga.saycare;

import static org.diverproject.util.MessageUtil.die;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.sql.MySQL;

public class TabelaDadosLabels
{
	private static final MySQL mysql = new MySQL();

	static
	{
		mysql.setHost("localhost");
		mysql.setDatabase("saycare_release");
		mysql.setUsername("saycare");
		mysql.setPassword("saycare2015");

		try {
			mysql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	public static void main(String args[]) throws Exception
	{
		ler("Labels/Dados Gerais Adolescentes.txt",	1, 2);
		ler("Labels/Dados Gerais Escolares.txt",	1, 0, 1);
		ler("Labels/Antropometria.txt",				3, 0, 1, 2);
		ler("Labels/Press�o Arterial.txt",			4, 0, 1, 2);
		ler("Labels/Di�rio do Aceler�metro.txt",	5, 0, 1, 2);
		ler("Labels/Pais.txt",						6, 0, 1, 2);
		ler("Labels/Sa�de Bucal.txt",				7, 0, 1, 2);
		ler("Labels/Matura��o Femenina.txt",		8, 1, 2);
		ler("Labels/Matura��o Masculina.txt",		8, 1, 2);
		ler("Labels/Ficha Bucal.txt",				11, 0, 1, 2);
		ler("Labels/FFQ.txt",						13, 0, 1, 2);
		ler("Labels/Aceler�metros.txt",				14, 0, 1, 2);
		ler("Labels/Recordatorio.txt",				15, 0, 1, 2);
	}

	public static void ler(String filepath, int qid, int... ages) throws Exception
	{
		for (int age : ages)
			ler(filepath, qid, age);
	}

	public static void ler(String filepath, int qid, int age) throws Exception
	{
		List<String> labels = new DynamicList<>();

		File file = new File(filepath);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		while (br.ready())
			labels.add(br.readLine());

		br.close();

		validar(labels);
		criar(qid, age, labels);
	}

	private static void validar(List<String> labels)
	{
		for (int i = 0; i < labels.size(); i++)
			for (int j = i; j < labels.size(); j++)
				if (i != j && labels.get(i).equals(labels.get(j)))
					System.out.println((i + 1)+ ": " +labels.get(i));		
	}

	private static void criar(int qid, int age, List<String> labels)
	{
		String sql = "REPLACE INTO labels (questionario, age_group, enum, name) VALUES ";

		for (int i = 0; i < labels.size(); i++)
		{
			sql += String.format("(%d, %d, %d, '%s')", qid, age, i+1, labels.get(i));

			if (i == labels.size() - 1)
				sql += ";";
			else
				sql += ",";
		}

		Connection connection = mysql.getConnection();

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			System.out.printf("%d labels registradas (qid: %d, age_group: %d).\n", ps.executeUpdate(), qid, age);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
