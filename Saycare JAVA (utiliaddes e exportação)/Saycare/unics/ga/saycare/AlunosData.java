package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;

import tk.saycare.Conexao;

public class AlunosData
{
	public static void main(String[] args) throws IOException, SQLException
	{
		List<Integer> ids = identificar("Alunos.txt");
		List<Aluno> alunos = carregar(ids);

		imprimir(alunos);
	}

	private static List<Integer> identificar(String filePath) throws IOException
	{
		File file = new File(filePath);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);

		List<Integer> ids = new DynamicList<>();

		for (int i = 0; br.ready(); i++)
		{
			String line = br.readLine();

			if (i == 0)
				continue;

			if (IntUtil.isInteger(line))
				ids.add(Integer.parseInt(line));
		}

		br.close();

		return ids;
	}

	private static List<Aluno> carregar(List<Integer> ids) throws SQLException
	{
		Connection connection = Conexao.getConnection();

		List<Aluno> alunos = new DynamicList<>();
		String sql = "SELECT respostas.resposta FROM respostas "
					+"INNER JOIN alunos ON alunos.id = respostas.aluno "
					+"WHERE respostas.aluno = ? AND respostas.entrada = 1 AND respostas.questionario = 1 "
					+"AND respostas.pergunta = 2 AND respostas.ordem = 1";

		for (int id : ids)
		{
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			Aluno aluno = new Aluno();
			alunos.add(aluno);
			aluno.id = id;

			if (rs.next())
				aluno.data = rs.getString("resposta");
		}

		return alunos;
	}

	private static void imprimir(List<Aluno> alunos) throws IOException
	{
		System.out.printf("ID_number\tdata\n");

		for (Aluno aluno : alunos)
			System.out.printf("%d\t%s\n", aluno.id, aluno.data);
	}

	private static class Aluno
	{
		private int id;
		private String data;
	}
}
