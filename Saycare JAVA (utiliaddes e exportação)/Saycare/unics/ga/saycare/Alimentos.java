package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;

public class Alimentos
{
	public static void main(String[] args) throws IOException
	{
		File file = new File("Alimentos.txt");
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		List<String> lines = new DynamicList<>();

		while (br.ready())
		{
			String line = br.readLine();

			if (!lines.contains(line.toLowerCase()))
				lines.add(line.toLowerCase());
			else
				System.out.println(line);
		}

		br.close();
	}
}
