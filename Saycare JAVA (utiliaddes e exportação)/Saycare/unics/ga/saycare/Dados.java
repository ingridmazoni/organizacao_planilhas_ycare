package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;

public class Dados
{
	public static void main(String[] args) throws IOException
	{
		List<Integer> ids = lerIDs("DadosIDs.txt");
		filtrar(ids, "Dados.txt");
	}

	private static List<Integer> lerIDs(String filepath) throws IOException
	{
		File file = new File(filepath);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);
		List<Integer> ids = new DynamicList<>();

		while (br.ready())
		{
			String line = br.readLine();
			int id = IntUtil.parse(line);
			ids.add(id);
		}

		br.close();

		return ids;
	}

	private static void filtrar(List<Integer> ids, String filepath) throws IOException
	{
		File file = new File(filepath);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		while (br.ready())
		{
			String line = br.readLine();

			if (line.isEmpty())
				continue;

			String columns[] = line.split("\t");
			int current = IntUtil.parse(columns[0]);
			boolean has = false;

			for (int id : ids)
				if (id == current)
				{
					has = true;
					break;
				}

			if (!has)
				System.out.println(line);
		}

		br.close();
	}
}
