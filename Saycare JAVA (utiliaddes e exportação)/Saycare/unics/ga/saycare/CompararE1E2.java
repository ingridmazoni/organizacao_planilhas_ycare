package ga.saycare;

import static org.diverproject.util.Util.format;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.Map.MapItem;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;
import org.diverproject.util.collection.abstraction.StringSimpleMap;
import org.diverproject.util.lang.DoubleUtil;
import org.diverproject.util.lang.FloatUtil;

public class CompararE1E2
{
	private static Map<Integer, StringSimpleMap<String>> alunos;
	private static List<String> labels;

	public static void main(String args[]) throws IOException
	{
		processar("Dados.txt");
	}

	private static void processar(String filename) throws IOException
	{
		alunos = new IntegerLittleMap<>();
		labels = new DynamicList<>();

		lerQeustionarioTabela(filename);
		comparar();
	}

	private static void lerQeustionarioTabela(String filename) throws IOException
	{
		File file = new File(filename);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		String colunas[] = br.readLine().split("\t");

		for (int i = 0; i < colunas.length; i++)
		{
			if (colunas[i] == null)
			{
				br.close();
				throw new IOException(format("coluna %d n�o definida", i + 1));
			}

			labels.add(colunas[i].trim());
		}

		for (int i = 2; br.ready(); i++)
		{
			String linha = br.readLine();
			String valores[] = linha.split("\t");

			if (valores.length != labels.size())
			{
				br.close();
				throw new IOException(format("linha com %d valores de %d (linha: %d)", valores.length, labels.size(), i));
			}

			int alunoID = Integer.parseInt(valores[0]);
			StringSimpleMap<String> respostas = new StringSimpleMap<>();
			{
				for (int j = 0; j < valores.length; j++)
				{
					String label = labels.get(j);
					respostas.add(label, valores[j].trim());
				}
			}

			if (alunos.add(alunoID, respostas))
				System.out.printf("Lido %d com %d respostas.\n", alunoID, respostas.size());
			else
			{
				br.close();
				throw new IOException(format("falha ao registrar respostas do aluno %d", alunoID));
			}
		}

		br.close();
	}

	private static void comparar()
	{
		int[][] inconsistencias = new int[7][2];

		for (MapItem<Integer, StringSimpleMap<String>> aluno : alunos.iterateItems())
		{
			for (int i = 0; i < labels.size(); i++)
			{
				String label = labels.get(i);
				String resposta = aluno.value.get(label);

				if (label.endsWith("_E1"))
				{
					String anotherLabel = label.substring(0, label.length() - 3)+ "_E2";
					String anotherResposta = aluno.value.get(anotherLabel);
					int cidade = Integer.parseInt(Character.toString(Integer.toString(aluno.key).charAt(0))) - 1;

					if (!resposta.equals(anotherResposta))
						inconsistencias[cidade][0]++;
					else
						inconsistencias[cidade][1]++;
				}
			}
		}

		for (int i = 0; i < inconsistencias.length; i++)
		{
			int inconsistente = inconsistencias[i][0];
			int total = inconsistencias[i][0] + inconsistencias[i][1];

			System.out.printf("cidade %d: %d de %d (%.2f%%)\n", i + 1, inconsistente, total, FloatUtil.toPorcent(inconsistente, total));
		}
	}

	public static float avarage(float first, float second, float third)
	{
		if (second == 0.0f)
			return first;

		if (onInterval(first, second) || third == 0.0f)
			return (first + second) / 2;

		if (onInterval(first, third))
			return (first + third) / 2;

		if (onInterval(second, third))
			return (second + third) / 2;

		return (first + second + third) / 3;
	}

	private static boolean onInterval(float a, float b)
	{
		double percentual = DoubleUtil.toPorcent(a, b);

		return percentual >= 95 && percentual <= 105;
	}
}
