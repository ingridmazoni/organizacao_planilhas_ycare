package ga.saycare;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.diverproject.util.DateUtil;
import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;

public class DatasDeNascimento
{
	public static void main(String[] args) throws IOException, ParseException
	{
		List<Aluno> alunos = lerAntropometria("Antropometria.txt");

		lerDadosGerais("Dados Gerais.txt", alunos);
		escreverDados("Datas Nascimento.txt", alunos);
	}

	private static List<Aluno> lerAntropometria(String filepath) throws IOException, ParseException
	{
		List<Aluno> alunos = new DynamicList<>();

		File file = new File(filepath);
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		while (bufferedReader.ready())
		{
			String linha = bufferedReader.readLine();
			String colunas[] = linha.split("\t");

			Aluno aluno = new Aluno();
			aluno.id = IntUtil.parse(colunas[0]);
			aluno.faixa = colunas[1];
			alunos.add(aluno);

			if (!colunas[2].equals("."))
				aluno.avaliacao = DateUtil.toDate(colunas[2]);
		}

		bufferedReader.close();

		return alunos;
	}

	private static void lerDadosGerais(String filepath, List<Aluno> alunos) throws IOException, ParseException
	{
		File file = new File(filepath);
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		while (bufferedReader.ready())
		{
			String linha = bufferedReader.readLine();
			String colunas[] = linha.split("\t");

			int id = IntUtil.parse(colunas[0]);

			for (Aluno aluno : alunos)
				if (aluno.id == id)
				{
					aluno.sexo = IntUtil.parse(colunas[1]);

					if (!colunas[2].equals("."))
						aluno.nascimento = DateUtil.toDate(colunas[2]);
				}
		}

		bufferedReader.close();
	}

	private static void escreverDados(String filepath, List<Aluno> alunos) throws IOException
	{
		File file = new File(filepath);
		FileWriter fileWriter = new FileWriter(file);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		bufferedWriter.write("AlunoID\tFaixa\tSexo\tDataNascimento\tDataAvaliacao");

		for (Aluno aluno : alunos)
		{
			bufferedWriter.newLine();
			bufferedWriter.write(Integer.toString(aluno.id));
			bufferedWriter.write("\t");
			bufferedWriter.write(aluno.faixa != null ? aluno.faixa : "");
			bufferedWriter.write("\t");
			bufferedWriter.write(aluno.sexo != 0 ? Integer.toString(aluno.sexo) : "");
			bufferedWriter.write("\t");
			bufferedWriter.write(aluno.nascimento != null ? DateUtil.toString(aluno.nascimento) : "");
			bufferedWriter.write("\t");
			bufferedWriter.write(aluno.avaliacao != null ? DateUtil.toString(aluno.avaliacao) : "");
		}

		bufferedWriter.close();
	}

	private static class Aluno
	{
		private int id;
		private int sexo;
		private String faixa;
		private Date avaliacao;
		private Date nascimento;
	}
}
