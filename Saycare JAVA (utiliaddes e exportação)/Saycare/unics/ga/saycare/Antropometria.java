package ga.saycare;

import static org.diverproject.util.Util.format;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.Map.MapItem;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;
import org.diverproject.util.collection.abstraction.StringSimpleMap;
import org.diverproject.util.lang.DoubleUtil;
import org.diverproject.util.lang.FloatUtil;

public class Antropometria
{
	private static Map<Integer, StringSimpleMap<String>> alunos;
	private static List<String> labels;

	public static void main(String args[]) throws IOException
	{
		processar("Dados.txt");
	}

	private static void processar(String filename) throws IOException
	{
		alunos = new IntegerLittleMap<>();
		labels = new DynamicList<>();

		lerQeustionarioTabela(filename);
		escreverQuestionarioTabela(filename);
	}

	private static void lerQeustionarioTabela(String filename) throws IOException
	{
		File file = new File(filename);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		String colunas[] = br.readLine().split("\t");

		for (int i = 0; i < colunas.length; i++)
		{
			if (colunas[i] == null)
			{
				br.close();
				throw new IOException(format("coluna %d n�o definida", i + 1));
			}

			labels.add(colunas[i].trim());
		}

		for (int i = 2; br.ready(); i++)
		{
			String linha = br.readLine();
			String valores[] = linha.split("\t");

			if (valores.length != labels.size())
			{
				br.close();
				throw new IOException(format("linha com %d valores de %d (linha: %d)", valores.length, labels.size(), i));
			}

			int alunoID = Integer.parseInt(valores[0]);
			StringSimpleMap<String> respostas = new StringSimpleMap<>();
			{
				for (int j = 0; j < valores.length; j++)
				{
					String label = labels.get(j);
					respostas.add(label, valores[j].trim());
				}
			}

			if (alunos.add(alunoID, respostas))
				System.out.printf("Lido %d com %d respostas.\n", alunoID, respostas.size());
			else
			{
				br.close();
				throw new IOException(format("falha ao registrar respostas do aluno %d", alunoID));
			}
		}

		br.close();
	}

	private static void escreverQuestionarioTabela(String filename) throws IOException
	{
		File file = new File("Gen"+filename);
		FileWriter writer = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(writer);

		for (MapItem<Integer, StringSimpleMap<String>> aluno : alunos.iterateItems())
		{
			for (int i = 0; i < labels.size(); i++)
			{
				String label = labels.get(i);
				String resposta = aluno.value.get(label);

				if (resposta == null)
				{
					bw.close();
					throw new IOException(format("reposta para '%s' n�o encontrado no aluno %d", label, aluno.key));
				}

				resposta = resposta.replace(',', '.');

				if (label.endsWith("a") || label.endsWith("b") || label.endsWith("c") || label.endsWith("d"))
				{
					float floatValue = resposta.equals(".") ? 0.0f : FloatUtil.parse(resposta, -1f);

					if (label.endsWith("d"))
					{
						String labelA = label.substring(0, label.length() - 1)+ "a";
						String labelB = label.substring(0, label.length() - 1)+ "b";
						String labelC = label.substring(0, label.length() - 1)+ "c";

						float first = FloatUtil.parse(aluno.value.get(labelA).replaceAll(",", "."));
						float second = FloatUtil.parse(aluno.value.get(labelB).replaceAll(",", "."));
						float third = FloatUtil.parse(aluno.value.get(labelC).replaceAll(",", "."));

						floatValue = avarage(first, second, third);
					}

					if (floatValue == -1f)
					{
						bw.close();
						throw new IOException(format("resposta para '%s' possui valor '%s' n�o float", label, resposta));
					}

					if (floatValue > 0)
						resposta = String.format("%.2f", floatValue);
					else
						resposta = ".";

					if (!aluno.value.update(label, resposta))
					{
						bw.close();
						throw new IOException(format("falha ao atualizar '%s' para '%s'", label, resposta));
					}
				}

				bw.write(resposta);

				if (i != labels.size() - 1)
					bw.write("\t");
			}

			bw.newLine();
			bw.flush();

			System.out.printf("Dados do aluno %d escritos.\n", aluno.key);
		}

		bw.close();
	}

	public static float avarage(float first, float second, float third)
	{
		if (second == 0.0f)
			return first;

		if (onInterval(first, second) || third == 0.0f)
			return (first + second) / 2;

		if (onInterval(first, third))
			return (first + third) / 2;

		if (onInterval(second, third))
			return (second + third) / 2;

		return (first + second + third) / 3;
	}

	private static boolean onInterval(float a, float b)
	{
		double percentual = DoubleUtil.toPorcent(a, b);

		return percentual >= 95 && percentual <= 105;
	}
}
