package ga.saycare;

import static org.diverproject.util.MessageUtil.die;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.sql.MySQL;

public class RealatorioQuestionariosTotais
{
	private static final MySQL sql = new MySQL();

	static
	{
		sql.setHost("localhost");
		sql.setDatabase("saycare_release");
		sql.setUsername("saycare");
		sql.setPassword("saycare2015");

		try {
			sql.connect();
		} catch (SQLException e) {
			die(e);
		} catch (ClassNotFoundException e) {
			die(e);
		}
	}

	public static void main(String[] args)
	{
		loadCity(new City(1, "S�o Paulo"));
		loadCity(new City(2, "Teresina"));
		loadCity(new City(3, "Buenos Aires"));
		loadCity(new City(4, "Medellin"));
		loadCity(new City(5, "Lima"));
		loadCity(new City(6, "Montevideo"));
		loadCity(new City(7, "Santiago"));
	}

	private static void loadCity(City city)
	{
		loadAgeQ1(0, city);
		loadAgeQ2(0, city);
		loadAgeQ1(1, city);
		loadAgeQ2(1, city);
		loadAgeQ1(2, city);
		loadAgeQ2(2, city);

		printCity(city);
	}

	private static void loadAgeQ1(int age, City city)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT COUNT(DISTINCT aluno) as size "
					+"FROM respostas "
					+"INNER JOIN alunos ON alunos.id = respostas.aluno "
					+"INNER JOIN labels ON labels.id = respostas.label "
					+"WHERE alunos.age_group = ? AND alunos.id >= ? AND alunos.id <= ? AND "
					+"(labels.name = 'dp_sex_Q1' OR labels.name = 'ant1' OR labels.name = 'pa1' OR labels.name = 'ffqcma_Q1')";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, age);
			ps.setInt(2, city.getMinAlunoID());
			ps.setInt(3, city.getMaxAlunoID());

			ResultSet rs = ps.executeQuery();

			if (rs.next())
				city.count[age][0] = rs.getInt("size");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void loadAgeQ2(int age, City city)
	{
		Connection connection = sql.getConnection();
		String sql = "SELECT COUNT(DISTINCT aluno) as size "
					+"FROM respostas "
					+"INNER JOIN alunos ON alunos.id = respostas.aluno "
					+"INNER JOIN labels ON labels.id = respostas.label "
					+"WHERE alunos.age_group = ? AND alunos.id >= ? AND alunos.id <= ? AND "
					+"(labels.name = 'dp_sex_Q2' OR labels.name = 'fs1a_Q2' OR labels.name = 'ffqcma_Q2' OR labels.name = 'ant1' OR labels.name = 'pa1')";

		try {

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, age);
			ps.setInt(2, city.getMinAlunoID());
			ps.setInt(3, city.getMaxAlunoID());

			ResultSet rs = ps.executeQuery();

			if (rs.next())
				city.count[age][1] = rs.getInt("size");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void printCity(City city)
	{
		System.out.printf("%16s: ", city.name);
		System.out.printf("%3d\t%3d\t", city.count[0][0], city.count[0][1]);
		System.out.printf("%3d\t%3d\t", city.count[1][0], city.count[1][1]);
		System.out.printf("%3d\t%3d\n", city.count[2][0], city.count[2][1]);
	}

	private static class City
	{
		private int center;
		private String name;
		private int count[][] = new int[3][2];

		public City(int center, String name)
		{
			this.center = center;
			this.name = name;
		}

		public int getMinAlunoID()
		{
			return Integer.parseInt(String.format("%d000000", center));
		}

		public int getMaxAlunoID()
		{
			return Integer.parseInt(String.format("%d999999", center));
		}
	}
}
