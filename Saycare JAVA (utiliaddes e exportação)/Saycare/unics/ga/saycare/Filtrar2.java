package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.abstraction.DynamicList;
import org.diverproject.util.lang.IntUtil;

public class Filtrar2
{
	public static void main(String[] args) throws IOException
	{
		List<Integer> ids = getAlunoIDs("Alunos.txt");
		filtrar(ids, "Dados.txt");
	}

	private static List<Integer> getAlunoIDs(String filepath) throws IOException
	{
		List<Integer> ids = new DynamicList<>();

		File file = new File(filepath);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		br.readLine();

		while (br.ready())
		{
			String line = br.readLine();
			int id = Integer.parseInt(line);

			for (int i = 0; i < ids.size(); i++)
				if (ids.get(i).equals(id))
					continue;

			ids.add(id);
		}

		br.close();

		return ids;
	}

	private static void filtrar(List<Integer> ids, String filepath) throws IOException
	{
		File file = new File(filepath);
		FileReader in = new FileReader(file);
		BufferedReader br = new BufferedReader(in);

		System.out.println(br.readLine());

		while (br.ready())
		{
			String line = br.readLine();
			int idRead = IntUtil.parse(line.split("\t")[0]);
			boolean contains = false;

			for (int id : ids)
				if (idRead == id)
				{
					contains = true;
					break;
				}

			if (contains)
				System.out.println(line);
		}

		br.close();
	}
}
