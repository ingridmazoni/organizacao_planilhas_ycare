package ga.saycare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;

import org.diverproject.util.lang.DoubleUtil;
import org.diverproject.util.lang.FloatUtil;

public class TerceiraMedida
{
	public static void main(String[] args) throws IOException
	{
		File file = new File("Medidas.txt");
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);

		for (int i = 0; br.ready(); i++)
		{
			String line = br.readLine().replace(',', '.');

			if (i == 0)
				continue;

			if (line.equals("		"))
			{
				System.out.println("		");
				continue;
			}

			String columns[] = line.split("\t");

			float valueA = FloatUtil.parse(columns[0]);
			float valueB = FloatUtil.parse(columns[1]);
			float valueC = FloatUtil.parse(columns[2]);

			float finalValue = 0f;
			boolean blank = false;

			if (valueA == 0f && valueB == 0f && valueC == 0f)
				blank = true;

			else if (valueA == 0f || valueB == 0f)
				blank = true;

			else if (!onInterval(valueA, valueB))
			{
				if (valueC == 0)
					blank = true;

				else if (onInterval(valueA, valueC))
					finalValue = (valueA + valueC) / 2;

				else if (onInterval(valueB, valueC))
					finalValue = (valueB + valueC) / 2;

				else
					blank = true;
			}

			else
				finalValue = (valueA + valueB) / 2;

			if (blank)
				System.out.printf(Locale.US, ".\t.\t.\t.\n");
			else
				System.out.printf(Locale.US, "%.2f\t%.2f\t%s\t%.2f\n", valueA, valueB, valueC > 0 ? Float.toString(valueC) : ".", finalValue);
		}

		br.close();
	}

	private static boolean onInterval(float a, float b)
	{
		double percentual = DoubleUtil.toPorcent(a, b);

		return percentual >= 95 && percentual <= 105;
	}
}
