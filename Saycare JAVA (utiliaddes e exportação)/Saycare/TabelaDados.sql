CREATE TABLE alunos
(
	id INT NOT NULL,
	center INT NOT NULL,
	age_group INT NOT NULL,
	school INT NOT NULL,

	PRIMARY KEY (id)
);

CREATE TABLE labels
(
	id INT NOT NULL AUTO_INCREMENT,
	questionario INT NOT NULL,
	age_group INT NOT NULL,
	enum INT NOT NULL,
	name VARCHAR(32),

	UNIQUE (questionario, age_group, name),
	PRIMARY KEY (id, name)
);

CREATE TABLE respostas
(
	aluno INT NOT NULL,
	label INT NOT NULL,
	valor VARCHAR(128) NOT NULL,

	PRIMARY KEY (aluno, label),
	FOREIGN KEY (aluno) REFERENCES alunos(id),
	FOREIGN KEY (label) REFERENCES labels(id)
);

DELETE FROM labels WHERE 1;
ALTER TABLE labels AUTO_INCREMENT = 1;

ID_number	center	age_group1	school

/*

SELECT labels.age_group, COUNT(DISTINCT(respostas.aluno)) as contagem
FROM respostas 
INNER JOIN labels ON labels.id = respostas.label 
WHERE (labels.name = 'dp_sex_Q1' OR labels.name = 'ant1' OR labels.name = 'pa1' OR labels.name = 'fs1a_Q1') AND respostas.aluno >= 1000000 AND respostas.aluno <= 1999999
GROUP BY labels.age_group 
ORDER BY respostas.aluno, labels.age_group
LIMIT 0, 1000;

SELECT labels.age_group, COUNT(DISTINCT(respostas.aluno)) as contagem
FROM respostas 
INNER JOIN labels ON labels.id = respostas.label 
WHERE (labels.name = 'dp_sex_Q2' OR labels.name = 'fs1a_Q2') AND respostas.aluno >= 1000000 AND respostas.aluno <= 1999999
GROUP BY labels.age_group 
ORDER BY respostas.aluno, labels.age_group
LIMIT 0, 1000;

*/