package tk.saycare.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.diverproject.util.FileUtil;

public class SaycareUtil
{
	public static BufferedWriter newBufferedWriter(String categoria, String filename) throws IOException
	{
		File file = getFile(categoria, filename);
		FileUtil.makeDirs(file);

		FileOutputStream fos = new FileOutputStream(file);
		OutputStreamWriter out = new OutputStreamWriter(fos, "Cp1252");
		BufferedWriter bw = new BufferedWriter(out);

		return bw;
	}

	public static File getFile(String categoria, String filename)
	{
		return new File("D:/Driw/Workspace/Java/Saycare/" +categoria, filename);
	}
}
