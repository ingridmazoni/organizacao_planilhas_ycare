package tk.saycare.util;

import java.util.Iterator;

import org.diverproject.util.ObjectDescription;
import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.Queue;
import org.diverproject.util.collection.abstraction.DynamicQueue;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;

import tk.saycare.entidades.Entrada;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;

public class MapSessao implements Map<SessaoKey, Sessao>
{
	private int size;
	private Map<Integer, Map<Integer, Entrada>> entradasPorAluno;

	public MapSessao()
	{
		clear();
	}

	@Override
	public boolean add(SessaoKey key, Sessao sessao)
	{
		if (isFull())
			return false;

		if (!entradasPorAluno.containsKey(key.getUsuario().getId()))
			entradasPorAluno.add(key.getUsuario().getId(), new IntegerLittleMap<>());

		Map<Integer, Entrada> entradas = entradasPorAluno.get(key.getUsuario().getId());

		if (!entradas.containsKey(key.getAluno().getId()))
			entradas.add(key.getAluno().getId(), new Entrada());

		Entrada entrada = entradas.get(key.getAluno().getId());

		if (entrada.hasSessao(key.getEntrada(), key.getQuestionario()))
			return false;

		entrada.setSessao(key.getEntrada(), sessao);
		size++;

		return true;
	}

	@Override
	public Sessao get(SessaoKey key)
	{
		if (!entradasPorAluno.containsKey(key.getUsuario().getId()))
			entradasPorAluno.add(key.getUsuario().getId(), new IntegerLittleMap<>());

		Map<Integer, Entrada> entradas = entradasPorAluno.get(key.getUsuario().getId());

		if (entradas != null)
		{
			if (!entradas.containsKey(key.getAluno().getId()))
				entradas.add(key.getAluno().getId(), new Entrada());

			Entrada entrada = entradas.get(key.getAluno().getId());

			if (entrada != null)
				return entrada.getSessao(key.getEntrada(), key.getQuestionario());
		}

		return null;
	}

	@Override
	public void clear()
	{
		entradasPorAluno = new IntegerLittleMap<>();
	}

	@Override
	public int size()
	{
		return size;
	}

	@Override
	public int length()
	{
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean isEmpty()
	{
		return size == 0;
	}

	@Override
	public boolean isFull()
	{
		return size == length();
	}

	@Override
	public boolean contains(Sessao sessao)
	{
		if (isFull())
			return false;

		SessaoKey key = sessao.getKey();

		if (!entradasPorAluno.containsKey(key.getUsuario().getId()))
			entradasPorAluno.add(key.getUsuario().getId(), new IntegerLittleMap<>());

		Map<Integer, Entrada> entradas = entradasPorAluno.get(key.getUsuario().getId());

		if (!entradas.containsKey(key.getAluno().getId()))
			entradas.add(key.getAluno().getId(), new Entrada());

		Entrada entrada = entradas.get(key.getAluno().getId());

		if (entrada.hasSessao(key.getEntrada(), key.getQuestionario()))
			return false;

		return true;
	}

	@Override
	public Sessao[] toArray()
	{
		Queue<Sessao> queue = new DynamicQueue<>();

		for (Map<Integer, Entrada> entradas : entradasPorAluno)
			for (Entrada entrada : entradas)
			{
				Questionario questionario = new Questionario();

				for (int entradaIndex = 1; entradaIndex <= Entrada.MAX; entradaIndex++)
					for (int questionarioIndex = 1; questionarioIndex <= Questionario.MAX; questionarioIndex++)
					{
						questionario.setId(questionarioIndex);

						Sessao sessao = entrada.getSessao(entradaIndex, questionario);

						if (sessao != null)
							queue.offer(sessao);
					}
			}

		Sessao sessoes[] = new Sessao[queue.size()];

		for (int i = 0; i < sessoes.length; i++)
			sessoes[i] = queue.poll();

		return sessoes;
	}

	@Override
	public Iterator<Sessao> iterator()
	{
		return new Iterator<Sessao>()
		{
			private int i = 0;
			private Sessao sessoes[] = toArray();

			@Override
			public Sessao next()
			{
				return sessoes[i++];
			}
			
			@Override
			public boolean hasNext()
			{
				return i < sessoes.length;
			}
		};
	}

	@Override
	public boolean containsKey(SessaoKey key)
	{
		Sessao sessoes[] = toArray();

		for (Sessao sessao : sessoes)
			if (sessao.getKey().equals(key))
				return true;

		return false;
	}

	@Override
	public boolean remove(Sessao sessao)
	{
		if (isEmpty())
			return false;

		SessaoKey key = sessao.getKey();

		if (!entradasPorAluno.containsKey(key.getUsuario().getId()))
			entradasPorAluno.add(key.getUsuario().getId(), new IntegerLittleMap<>());

		Map<Integer, Entrada> entradas = entradasPorAluno.get(key.getUsuario().getId());

		if (entradas == null)
			return false;

		Entrada entrada = entradas.get(key.getAluno().getId());

		if (!entrada.hasSessao(key.getEntrada(), key.getQuestionario()))
			return false;

		entrada.setSessao(key.getEntrada(), null);

		return true;
	}

	@Override
	public boolean removeKey(SessaoKey key)
	{
		if (isEmpty())
			return false;

		if (!entradasPorAluno.containsKey(key.getUsuario().getId()))
			entradasPorAluno.add(key.getUsuario().getId(), new IntegerLittleMap<>());

		Map<Integer, Entrada> entradas = entradasPorAluno.get(key.getUsuario().getId());

		if (entradas == null)
			return false;

		Entrada entrada = entradas.get(key.getAluno().getId());

		if (!entrada.hasSessao(key.getEntrada(), key.getQuestionario()))
			return false;

		entrada.setSessao(key.getEntrada(), null);

		return true;
	}

	@Override
	@Deprecated
	public Iterator<SessaoKey> iteratorKey()
	{
		return null;
	}

	@Override
	@Deprecated
	public Iterator<MapItem<SessaoKey, Sessao>> iteratorItems()
	{
		return null;
	}

	@Override
	@Deprecated
	public Class<?> getGeneric()
	{
		return null;
	}

	@Override
	@Deprecated
	public void setGeneric(Class<?> generic)
	{
		
	}

	@Override
	@Deprecated
	public boolean renameKey(SessaoKey oldKey, SessaoKey newKey)
	{
		return false;
	}

	@Override
	@Deprecated
	public boolean update(SessaoKey key, Sessao value)
	{
		return false;
	}

	@Override
	@Deprecated
	public Iterable<SessaoKey> iterateKey()
	{
		return null;
	}

	@Override
	@Deprecated
	public Iterable<MapItem<SessaoKey, Sessao>> iterateItems()
	{
		return null;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		Sessao sessoes[] = toArray();

		for (Sessao sessao : sessoes)
			description.append(sessao);

		return description.toString();
	}
}
