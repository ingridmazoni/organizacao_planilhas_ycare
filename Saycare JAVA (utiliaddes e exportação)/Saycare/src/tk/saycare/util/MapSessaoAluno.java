package tk.saycare.util;

import java.util.Iterator;

import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.IntegerLargeMap;
import org.diverproject.util.lang.IntUtil;

import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Entrada;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;

public class MapSessaoAluno implements Iterable<Sessao[]>
{
	private int size;
	private Map<Integer, Item> mapa = new IntegerLargeMap<>();

	public void adicionar(Sessao sessao)
	{
		SessaoKey key = sessao.getKey();
		Item item;

		if (mapa.containsKey(key.getAluno().getId()))
		{
			item = mapa.get(key.getAluno().getId());

			if (item.entradas[key.getQuestionario().getId() - 1][key.getEntrada() - 1] == null)
				size++;
		}

		else
		{
			item = new Item();

			mapa.add(key.getAluno().getId(), item);
			size++;
		}

		item.entradas[key.getQuestionario().getId() - 1][key.getEntrada() - 1] = sessao;
	}

	public void remover(Aluno aluno)
	{
		mapa.removeKey(aluno.getId());
	}

	public void remover(Aluno aluno, int entrada)
	{
		Item item = mapa.get(aluno.getId());

		if (item != null)
			if (IntUtil.interval(entrada, 1, item.entradas.length))
				item.entradas[entrada - 1] = null;
	}

	public Sessao[] getSessao(Aluno aluno, Questionario questionario)
	{
		Item item = mapa.get(aluno.getId());

		if (item != null)
			return item.entradas[questionario.getId() - 1];

		return null;
	}

	public Sessao getSessao(Aluno aluno, Questionario questionario, int entrada)
	{
		Item item = mapa.get(aluno.getId());

		if (item != null)
			if (IntUtil.interval(questionario.getId(), 1, Questionario.MAX))
				if (IntUtil.interval(entrada, 1, Entrada.MAX))
					return item.entradas[questionario.getId() - 1][entrada - 1];

		return null;
	}

	public int size()
	{
		return size;
	}

	@Override
	public Iterator<Sessao[]> iterator()
	{
		mapa.setGeneric(Item.class);

		return new Iterator<Sessao[]>()
		{
			private int indice;
			private int questionario;
			private Item[] itens = mapa.toArray();

			@Override
			public boolean hasNext()
			{
				return !(indice == itens.length - 1 && questionario < Questionario.MAX);
			}

			@Override
			public Sessao[] next()
			{
				Item item = itens[indice];

				while (item == null)
					item = itens[++indice];

				Sessao sessoes[] = item.entradas[questionario++];

				if (questionario == Questionario.MAX)
				{
					indice++;
					questionario = 0;
				}

				return sessoes;
			}
		};
	}

	private class Item
	{
		private final Sessao[][] entradas;
	
		private Item()
		{
			this.entradas = new Sessao[Questionario.MAX][Entrada.MAX];
		}
	}
}
