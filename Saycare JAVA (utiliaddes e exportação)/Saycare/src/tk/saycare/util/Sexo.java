package tk.saycare.util;

public enum Sexo
{
	FEMININO,
	MASCULINO,;

	public static Sexo parse(String valor)
	{
		switch (valor)
		{
			case "F": return FEMININO;
			case "M": return MASCULINO;
		}

		return null;
	}
}
