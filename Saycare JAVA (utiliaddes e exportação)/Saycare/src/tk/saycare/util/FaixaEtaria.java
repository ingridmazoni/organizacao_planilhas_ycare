package tk.saycare.util;

public enum FaixaEtaria
{
	ADOLESCENTE("ADO"),
	ESCOLAR("ESC"),
	PRE_ESCOLAR("PRE");

	public final String VALUE;

	private FaixaEtaria(String value)
	{
		VALUE = value;
	}

	public static FaixaEtaria parse(int faixa)
	{
		switch (faixa)
		{
			case 0: return ADOLESCENTE;
			case 1: return ESCOLAR;
			case 2: return PRE_ESCOLAR;
		}

		return null;
	}
}
