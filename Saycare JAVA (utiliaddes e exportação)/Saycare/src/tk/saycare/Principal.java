package tk.saycare;

import org.diverproject.util.collection.Map;

import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Escola;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.entidades.Usuario;
import tk.saycare.fronteira.exportacao.CarregarDadosBase;
import tk.saycare.fronteira.exportacao.ExportarColunasEmLabels;
import tk.saycare.fronteira.exportacao.ExportarRelatorioFaltaSessao;
import tk.saycare.fronteira.exportacao.ExportarRelatorioSessaoCidade;
import tk.saycare.fronteira.exportacao.ExportarRespostas;
import tk.saycare.fronteira.exportacao.ExportarSessoes;
import tk.saycare.fronteira.exportacao.ExportarToString;
import tk.saycare.fronteira.exportacao.VerificarRespostas;
import tk.saycare.fronteira.exportacao.VerificarRespostas2;
import tk.saycare.fronteira.exportacao.verificarSessoes;
import tk.saycare.util.MapSessaoAluno;

public class Principal
{
	public static void main(String args[]) throws Exception
	{
		Map<Integer, Escola> escolas;
		Map<Integer, Aluno> alunos;
		Map<Integer, Questionario> questionarios;
		Map<Integer, Usuario> usuarios;
		Map<SessaoKey, Sessao> sessoes;
		MapSessaoAluno respostas;

		CarregarDadosBase carregarDadosBase = new CarregarDadosBase();
		carregarDadosBase.call();

		questionarios = carregarDadosBase.getQuestionarios();
		usuarios = carregarDadosBase.getUsuarios();
		escolas = carregarDadosBase.getEscolas();
		alunos = carregarDadosBase.getAlunos();
		sessoes = carregarDadosBase.getSessoes();

		ExportarSessoes exportarSessoes = new ExportarSessoes();
		exportarSessoes.setSessoes(sessoes);
		exportarSessoes.call();

		ExportarRespostas exportarRespostas = new ExportarRespostas();
		exportarRespostas.setQuestionarios(questionarios);
		exportarRespostas.setSessoes(sessoes);
		exportarRespostas.setAlunos(alunos);
		exportarRespostas.call();

		respostas = exportarRespostas.getRespostas();

		verificarSessoes verificarSessoes = new verificarSessoes();
		verificarSessoes.call();

		VerificarRespostas verificarRespostas = new VerificarRespostas();
		verificarRespostas.setQuestionarios(questionarios);
		verificarRespostas.setRespostas(respostas);
		verificarRespostas.setSessoes(sessoes);
		verificarRespostas.setAlunos(alunos);
		verificarRespostas.call();

		ExportarToString exportarToString = new ExportarToString();
		exportarToString.setQuestionarios(questionarios);
		exportarToString.setUsuarios(usuarios);
		exportarToString.setEscolas(escolas);
		exportarToString.setAlunos(alunos);
		exportarToString.setSessoes(sessoes);
		exportarToString.call();

		ExportarRelatorioSessaoCidade exportarRelatorioSessaoCidade = new ExportarRelatorioSessaoCidade();
		exportarRelatorioSessaoCidade.setQuestionarios(questionarios);
		exportarRelatorioSessaoCidade.call();

		ExportarRelatorioFaltaSessao exportarRelatorioFaltaSessao = new ExportarRelatorioFaltaSessao();
		exportarRelatorioFaltaSessao.setQuestionarios(questionarios);
		exportarRelatorioFaltaSessao.call();

		ExportarColunasEmLabels exportarColunasEmLabels = new ExportarColunasEmLabels();
		exportarColunasEmLabels.call();

		VerificarRespostas2 verificarRespostas2= new VerificarRespostas2();
		verificarRespostas2.setAlunos(alunos);
		verificarRespostas2.setQuestionarios(questionarios);
		verificarRespostas2.setRespostas(respostas);
		verificarRespostas2.setSessoes(sessoes);
		verificarRespostas2.call();
	}
}
