package tk.saycare.fronteira;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.diverproject.util.MessageUtil;
import org.diverproject.util.SystemUtil;
import org.diverproject.util.collection.Map;

import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Escola;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.entidades.Usuario;
import tk.saycare.fronteira.exportacao.CarregarDadosBase;
import tk.saycare.fronteira.exportacao.ExportarColunasEmLabels;
import tk.saycare.fronteira.exportacao.ExportarRelatorioSessaoCidade;
import tk.saycare.fronteira.exportacao.ExportarRespostas;
import tk.saycare.fronteira.exportacao.ExportarSessoes;
import tk.saycare.fronteira.exportacao.ExportarToString;
import tk.saycare.fronteira.exportacao.VerificarRespostas;
import tk.saycare.fronteira.exportacao.verificarSessoes;
import tk.saycare.util.MapSessaoAluno;

@SuppressWarnings("serial")
public class Gerenciador extends JFrame
{
	private JLabel label;

	private Map<Integer, Escola> escolas;
	private Map<Integer, Aluno> alunos;
	private Map<Integer, Questionario> questionarios;
	private Map<Integer, Usuario> usuarios;
	private Map<SessaoKey, Sessao> sessoes;
	private MapSessaoAluno respostas;

	public Gerenciador()
	{
		setSize(320, 350);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(9, 1, 5, 5));
		setContentPane(panel);

		JButton btnCarregarBase = new JButton("Carregar Dados Base");
		btnCarregarBase.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callCarregarBase();
			}
		});
		panel.add(btnCarregarBase);

		JButton btnExportarSessoes = new JButton("Exportar Sess�es");
		btnExportarSessoes.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callExportarSessoes();
			}
		});
		panel.add(btnExportarSessoes);

		JButton btnExportarRespostas = new JButton("Exportar Respostas");
		btnExportarRespostas.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callExportarRespostas();
			}
		});
		panel.add(btnExportarRespostas);

		JButton btnVerfiicarSessoes = new JButton("Verificar Sess�es");
		btnVerfiicarSessoes.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callVerficarSessoes();
			}
		});
		panel.add(btnVerfiicarSessoes);

		JButton btnVerfiicarRespostas = new JButton("Verificar Respostas");
		btnVerfiicarRespostas.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callVerificarRespostas();
			}
		});
		panel.add(btnVerfiicarRespostas);

		JButton btnExportarToString = new JButton("Exportar ToString");
		btnExportarToString.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callExportarToString();
			}
		});
		panel.add(btnExportarToString);

		JButton btnExportarRelatorios = new JButton("Exportar Relat�rios");
		btnExportarRelatorios.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callExportarRelatorios();
			}
		});
		panel.add(btnExportarRelatorios);

		JButton btnColunasEmLabels = new JButton("Colunas em Labels");
		btnColunasEmLabels.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				callColunasEmLabels();
			}
		});
		panel.add(btnColunasEmLabels);

		label = new JLabel(getMemoryUsed());
		label.setHorizontalAlignment(JLabel.CENTER);
		panel.add(label);

		setVisible(true);
	}

	private String getMemoryUsed()
	{
		String usando = SystemUtil.getUsageMemoryString();
		String limite = SystemUtil.getAllocatedMemoryString();

		return String.format("Mem�ria Usada: %s de %s\n", usando, limite);
	}

	private void callAtualizarMemoriaUsada()
	{
		toFront();
		System.gc();

		label.setText(getMemoryUsed());
	}

	private void callCarregarBase()
	{
		try {

			CarregarDadosBase carregarDadosBase = new CarregarDadosBase();
			carregarDadosBase.call();

			questionarios = carregarDadosBase.getQuestionarios();
			usuarios = carregarDadosBase.getUsuarios();
			escolas = carregarDadosBase.getEscolas();
			alunos = carregarDadosBase.getAlunos();
			sessoes = carregarDadosBase.getSessoes();

			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}

	private void callExportarSessoes()
	{
		try {

			ExportarSessoes exportarSessoes = new ExportarSessoes();
			exportarSessoes.setSessoes(sessoes);
			exportarSessoes.call();

			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}

	private void callExportarRespostas()
	{
		try {

			ExportarRespostas exportarRespostas = new ExportarRespostas();
			exportarRespostas.setQuestionarios(questionarios);
			exportarRespostas.setSessoes(sessoes);
			exportarRespostas.setAlunos(alunos);
			exportarRespostas.call();

			respostas = exportarRespostas.getRespostas();

			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}

	private void callVerficarSessoes()
	{
		try {

			verificarSessoes verificarSessoes = new verificarSessoes();
			verificarSessoes.call();

			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}

		callAtualizarMemoriaUsada();
	}

	private void callVerificarRespostas()
	{
		try {

			VerificarRespostas verificarRespostas = new VerificarRespostas();
			verificarRespostas.setQuestionarios(questionarios);
			verificarRespostas.setRespostas(respostas);
			verificarRespostas.setSessoes(sessoes);
			verificarRespostas.setAlunos(alunos);
			verificarRespostas.call();

			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}

	private void callExportarToString()
	{
		try {

			ExportarToString exportarToString = new ExportarToString();
			exportarToString.setQuestionarios(questionarios);
			exportarToString.setUsuarios(usuarios);
			exportarToString.setEscolas(escolas);
			exportarToString.setAlunos(alunos);
			exportarToString.setSessoes(sessoes);
			exportarToString.call();
	
			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}

	private void callExportarRelatorios()
	{
		try {

			ExportarRelatorioSessaoCidade exportarRelatorios = new ExportarRelatorioSessaoCidade();
			exportarRelatorios.setQuestionarios(questionarios);
			exportarRelatorios.call();
	
			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}

	private void callColunasEmLabels()
	{
		try {

			ExportarColunasEmLabels exportarColunasEmLabels = new ExportarColunasEmLabels();
			exportarColunasEmLabels.call();
	
			callAtualizarMemoriaUsada();

		} catch (Exception e) {
			MessageUtil.showException(e);
			e.printStackTrace();
		}
	}
}
