package tk.saycare.fronteira.exportacao;

import static org.diverproject.util.Util.format;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.lang.IntUtil;

import tk.saycare.Conexao;

public class ContarDivergencias
{
	public static void main(String[] args)
	{
		String cities[] = new String[] { "Buenos Aires", "Lima", "Medell�n", "Montevid�u", "Santiago", "S�o Paulo", "Teresina" };

		for (String city : cities)
			readFrom("Diverg�ncias", city);

		System.out.println("---");

		readFieldCounts(cities);
	}

	private static void readFrom(String folderpath, String city)
	{
		File folder = new File(folderpath);

		int divergences = 0;
		int interpretatives = 0;

		if (folder.exists())
			for (File file : folder.listFiles())
				if (file.getName().toLowerCase().startsWith(city.toLowerCase()))
				{
					divergences += countDivergences(file);
					interpretatives += countInterpretatives(file);
				}

		System.out.printf("%d diverg�ncias sendo %d interpretativos em %s\n", divergences, interpretatives, city);
	}

	private static int countDivergences(File file)
	{
		int count = 0;

		try {

			FileReader in = new FileReader(file);
			BufferedReader reader = new BufferedReader(in);

			boolean ok = false;
			int alunoID = 0;

			while (reader.ready())
			{
				String line = reader.readLine();

				if (line.startsWith("-"))
				{
					String str = line.substring(12, 19);
					alunoID = IntUtil.parse(str);
					ok = contains(alunoID);
				}

				else if (line.startsWith("P") && ok)
					count++;
			}

			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return count;
	}

	private static int countInterpretatives(File file)
	{
		int count = 0;

		try {

			FileReader in = new FileReader(file);
			BufferedReader reader = new BufferedReader(in);

			boolean ok = false;
			int alunoID = 0;

			while (reader.ready())
			{
				String line = reader.readLine();

				if (line.startsWith("-"))
				{
					String str = line.substring(12, 19);
					alunoID = IntUtil.parse(str);
					ok = contains(alunoID);
				}

				else if (line.startsWith("P") && ok && (line.endsWith("[0] x [.]") || line.endsWith("[.] x [0]")))
					count++;
			}

			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return count;
	}

	private static boolean contains(int alunoID)
	{
		Connection connection = Conexao.getConnection();

		try {

			String sql = "SELECT COUNT(*) as count FROM alunos WHERE id = ?";

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, alunoID);

			ResultSet rs = ps.executeQuery();

			return rs.next() && rs.getInt("count") == 1;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	private static void readFieldCounts(String[] cities)
	{
		for (int i = 0; i < cities.length; i++)
			System.out.printf("%d campos em %s.\n", countFields(i + 1), cities[i]);
	}

	private static int countFields(int index)
	{
		Connection connection = Conexao.getConnection();

		try {

			int minAlunoID = IntUtil.parse(format("%d000000", index));
			int maxAlunoID = IntUtil.parse(format("%d999999", index));

			String sql = "SELECT COUNT(*) as count FROM respostas WHERE aluno >= ? AND aluno <= ?";

			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, minAlunoID);
			ps.setInt(2, maxAlunoID);

			ResultSet rs = ps.executeQuery();

			return rs.next() ? rs.getInt("count") : 0;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}
}
