package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;
import java.io.IOException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.lang.FloatUtil;

import tk.saycare.controles.ControleResposta;
import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Respostas;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.util.MapSessaoAluno;
import tk.saycare.util.SaycareUtil;

public class VerificarRespostas implements ICall
{
	private static final String CIDADES[] = new String[]
			{ "S�o Paulo", "Teresina", "Buenos Aires", "Medell�n", "Lima", "Montevideo", "Santiago" };

	private static final int VR_OK = 0;
	private static final int VR_PRIMEIRA = 1;
	private static final int VR_SEGUNDA = 2;
	private static final int VR_DUPLA = 3;
	private static final int VR_INCOMPLETO = 4;
	private static final int VR_INTERPRETACAO = 5;
	private static final int VR_DIFERENTE = 6;

	private Map<Integer, Aluno> alunos;
	private Map<Integer, Questionario> questionarios;
	private Map<SessaoKey, Sessao> sessoes;
	private MapSessaoAluno respostas;

	public Map<Integer, Aluno> getAlunos()
	{
		return alunos;
	}

	public void setAlunos(Map<Integer, Aluno> alunos)
	{
		this.alunos = alunos;
	}

	public Map<Integer, Questionario> getQuestionarios()
	{
		return questionarios;
	}

	public void setQuestionarios(Map<Integer, Questionario> questionarios)
	{
		this.questionarios = questionarios;
	}

	public Map<SessaoKey, Sessao> getSessoes()
	{
		return sessoes;
	}

	public void setSessoes(Map<SessaoKey, Sessao> sessoes)
	{
		this.sessoes = sessoes;
	}

	public MapSessaoAluno getRespostas()
	{
		return respostas;
	}

	public void setRespostas(MapSessaoAluno respostas)
	{
		this.respostas = respostas;
	}

	@Override
	public void call() throws Exception
	{
		ControleResposta controleResposta = new ControleResposta();
		respostas = controleResposta.mapear(sessoes);

		BufferedWriter bws[] = new BufferedWriter[CIDADES.length];

		for (int i = 0; i < bws.length; i++)
			bws[i] = SaycareUtil.newBufferedWriter("Verifica��es", CIDADES[i]+ ".txt");

		this.questionarios.setGeneric(Questionario.class);
		Questionario questionarios[] = this.questionarios.toArray();

		int validos = 0;
		int divergentes = 0;
		int interpretativos = 0;

		for (Questionario questionario : questionarios)
		{
			for (BufferedWriter writer : bws)
			{
				writer.newLine();
				writer.write(String.format("----- %s -----", questionario.getNome().toUpperCase()));
				writer.newLine();
				writer.newLine();
			}

			for (Aluno aluno : alunos)
			{
				Sessao entradas[] = respostas.getSessao(aluno, questionario);

				if (entradas != null && entradas[0] != null && entradas[1] != null)
				{
					int resultado = callVerificarRespostasDe(entradas[0], entradas[1]);
					BufferedWriter writer = bwCidade(entradas[0], bws);

					switch (resultado)
					{
						case VR_PRIMEIRA:
							writer.write(Integer.toString(entradas[0].getKey().getAluno().getId()));
							writer.write(": Respostas da segunda digita��o n�o encontrado.");
							writer.newLine();
							writer.flush();
							break;

						case VR_SEGUNDA:
							writer.write(Integer.toString(entradas[0].getKey().getAluno().getId()));
							writer.write(": Respostas da primeira digita��o n�o encontrado.");
							writer.newLine();
							writer.flush();
							break;

						case VR_DUPLA:
							writer.write(Integer.toString(entradas[0].getKey().getAluno().getId()));
							writer.write(": Respostas da primeira e segunda digita��o n�o encontrado.");
							writer.newLine();
							writer.flush();
							break;

						case VR_DIFERENTE:
						case VR_INTERPRETACAO:
							writer.write(Integer.toString(entradas[0].getKey().getAluno().getId()));
							writer.newLine();
							writer.flush();
							break;
					}
				}
			}
		}

		int total = validos + divergentes + interpretativos;
		int validosParcial = validos + interpretativos;

		System.out.println("Diverg�ncias conclu�das:");
		System.out.printf("\t- Validados: [%d de %d (%.2f%%)] total\n", validos, total, FloatUtil.toPorcent(validos, total));
		System.out.printf("\t- Divergentes: [%d de %d (%.2f%%)] total ou [%d de %d (%.2f%%)] parcial\n", divergentes, total, FloatUtil.toPorcent(divergentes, total), divergentes, validosParcial, FloatUtil.toPorcent(divergentes, validosParcial));
		System.out.printf("\t- Interpretativos: [%d de %d (%.2f%%)] total ou [%d de %d (%.2f%%)] parcial\n", interpretativos, total, FloatUtil.toPorcent(interpretativos, total), interpretativos, validos, FloatUtil.toPorcent(interpretativos, validos));
	}

	private int callVerificarRespostasDe(Sessao sessaoA, Sessao sessaoB) throws IOException
	{
		Respostas respostasA = sessaoA.getRespostas();
		Respostas respostasB = sessaoB.getRespostas();

		if (respostasA == null && respostasB == null)
			return VR_DUPLA;

		else if (respostasA == null)
			return VR_SEGUNDA;

		else if (respostasB == null)
			return VR_PRIMEIRA;

		else
		{
			for (int pergunta = 1; pergunta <= respostasA.getPerguntasCount(); pergunta++)
				for (int ordem = 1; ordem <= respostasA.getPerguntaOrdens(pergunta); ordem++)
				{
					String a = respostasA.getResposta(pergunta, ordem);
					String b = respostasB.getResposta(pergunta, ordem);

					if (a != null && a.isEmpty())	a = ".";
					if (b != null && b.isEmpty())	b = ".";

					if (a == null || b == null)
						return VR_INCOMPLETO;

					else if ((a.equals("0") && b.equals(".")) || (a.equals(".") && b.equals("0")))
						return VR_INTERPRETACAO;

					else if (!a.equalsIgnoreCase(b))
						return VR_DIFERENTE;
				}
		}

		return VR_OK;
	}

	private BufferedWriter bwCidade(Sessao sessao, BufferedWriter[] bws)
	{
		return bws[bwCidadeIndex(sessao)];
	}

	private int bwCidadeIndex(Sessao sessao)
	{
		int id = sessao.getKey().getAluno().getId();

		if (id < 1999999)
			return 0;

		if (id < 2999999)
			return 1;

		if (id < 3999999)
			return 2;

		if (id < 4999999)
			return 3;

		if (id < 5999999)
			return 4;

		if (id < 6999999)
			return 5;

		if (id < 7999999)
			return 6;

		return -1;
	}
}
