package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;
import java.io.IOException;

import org.diverproject.util.ObjectDescription;
import org.diverproject.util.collection.Map;

import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Escola;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.entidades.Usuario;
import tk.saycare.util.SaycareUtil;

public class ExportarToString implements ICall
{
	private static final String CATEGORIA = "Exporta��o";

	private Map<Integer, Escola> escolas;
	private Map<Integer, Aluno> alunos;
	private Map<Integer, Questionario> questionarios;
	private Map<Integer, Usuario> usuarios;
	private Map<SessaoKey, Sessao> sessoes;

	public Map<Integer, Escola> getEscolas()
	{
		return escolas;
	}

	public void setEscolas(Map<Integer, Escola> escolas)
	{
		this.escolas = escolas;
	}

	public Map<Integer, Aluno> getAlunos()
	{
		return alunos;
	}

	public void setAlunos(Map<Integer, Aluno> alunos)
	{
		this.alunos = alunos;
	}

	public Map<Integer, Questionario> getQuestionarios()
	{
		return questionarios;
	}

	public void setQuestionarios(Map<Integer, Questionario> questionarios)
	{
		this.questionarios = questionarios;
	}

	public Map<Integer, Usuario> getUsuarios()
	{
		return usuarios;
	}

	public void setUsuarios(Map<Integer, Usuario> usuarios)
	{
		this.usuarios = usuarios;
	}

	public Map<SessaoKey, Sessao> getSessoes()
	{
		return sessoes;
	}

	public void setSessoes(Map<SessaoKey, Sessao> sessoes)
	{
		this.sessoes = sessoes;
	}

	@Override
	public void call() throws Exception
	{
		exportarToString("Lista de Escolas.txt", escolas);
		exportarToString("Lista de Alunos.txt", alunos);
		exportarToString("Lista de Usuarios.txt", usuarios);
		exportarToString("Lista de Questionarios.txt", questionarios);
		exportarToString("Lista de Sessoes.txt", sessoes);
		exportarRespostasToString("Lista de respostas.txt", sessoes);		
	}

	private void exportarToString(String filename, Map<?, ?> map) throws IOException
	{
		if (map == null)
			return;

		BufferedWriter bw = SaycareUtil.newBufferedWriter(CATEGORIA, filename);

		for (Object object : map)
		{
			bw.write(object.toString());
			bw.newLine();
			bw.flush();
		}

		bw.close();

		System.out.printf("'%s' exportado em '%s'\n", filename, SaycareUtil.getFile(CATEGORIA, filename));
	}

	private void exportarRespostasToString(String filename, Map<SessaoKey, Sessao> sessoes) throws IOException
	{
		if (sessoes == null)
			return;

		BufferedWriter bw = SaycareUtil.newBufferedWriter(CATEGORIA, filename);

		for (Sessao sessao : sessoes)
		{
			ObjectDescription description = new ObjectDescription(sessao.getClass());

			SessaoKey key = sessao.getKey();

			description.append(key.getUsuario().getId());
			description.append(key.getAluno().getId());
			description.append(key.getQuestionario().getId());
			description.append(key.getEntrada());

			bw.write(description.toString());

			if (sessao.getRespostas() != null)
				bw.write(sessao.getRespostas().toString());

			bw.newLine();
			bw.flush();
		}

		bw.close();
		System.out.printf("'%s' exportado em '%s' com �xito!", filename, SaycareUtil.getFile(CATEGORIA, filename));
	}
}
