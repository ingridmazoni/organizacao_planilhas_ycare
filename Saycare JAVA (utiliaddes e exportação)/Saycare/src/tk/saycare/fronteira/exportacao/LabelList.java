package tk.saycare.fronteira.exportacao;

import org.diverproject.util.UtilRuntimeException;
import org.diverproject.util.collection.ArrayUtil;
import org.diverproject.util.collection.abstraction.DynamicList;

public class LabelList extends DynamicList<Label>
{
	@Override
	public boolean add(Label label)
	{
		if (label == null)
			return false;

		if (isFull())
			elements = ArrayUtil.increseIn(elements, DEFAULT_SIZE);

		for (int i = 0; i < size(); i++)
		{
			Label element = (Label) elements[i];

			if (element.getPergunta() > label.getPergunta() && element.getOrdem() > label.getOrdem())
				return add(label, i);

			if (element.getPergunta() == label.getPergunta() && element.getOrdem() == label.getOrdem())
				throw new UtilRuntimeException("label repetida P%d_O%d", element.getPergunta(), element.getOrdem());
		}

		elements[size++] = label;

		return true;
	}

	private boolean add(Label label, int index)
	{
		for (int i = size(); i >= index; i--)
			elements[i] = elements[i - 1];

		elements[index] = label;
		size++;

		return true;
	}
}
