package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;

import org.diverproject.util.DateUtil;
import org.diverproject.util.collection.Map;

import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.util.SaycareUtil;

public class ExportarSessoes implements ICall
{
	private Map<SessaoKey, Sessao> sessoes;

	public void setSessoes(Map<SessaoKey, Sessao> sessoes)
	{
		this.sessoes = sessoes;
	}

	@Override
	public void call() throws Exception
	{
		BufferedWriter bw = SaycareUtil.newBufferedWriter("Exporta��o", "sessoes.xls");

		bw.write("Usu�rio\tAlunoID\tAlunoSexo\tQuestion�rio\tEntrada\tHorario\tParte");
		bw.newLine();

		for (Sessao sessao : sessoes)
		{
			SessaoKey key = sessao.getKey();

			bw.write(key.getUsuario().getNome()+ " ");
			bw.write(key.getUsuario().getUltimoNome()+ "\t");
			bw.write(Integer.toString(key.getAluno().getId())+ "\t");
			bw.write(key.getAluno().getSexo().toString()+ "\t");
			bw.write(key.getQuestionario().getNome()+ "\t");
			bw.write(Integer.toString(key.getEntrada())+ "\t");
			bw.write(DateUtil.toString(sessao.getDate(), "dd/MM/yyyy HH:mm:ss")+ "\t");
			bw.write(Integer.toString(sessao.getParte()));
			bw.newLine();
		}

		System.out.printf("'sessoes.xls' exportado com �xtio!\n");
	}
}
