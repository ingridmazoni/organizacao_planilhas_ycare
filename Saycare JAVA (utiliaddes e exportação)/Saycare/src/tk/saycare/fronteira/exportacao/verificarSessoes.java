package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;

import org.diverproject.util.collection.List;

import tk.saycare.controles.ControleSessao;
import tk.saycare.entidades.Entrada;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.SessaoDivergencia;
import tk.saycare.util.SaycareUtil;

public class verificarSessoes implements ICall
{
	private static final String TAB = "\t";

	@Override
	public void call() throws Exception
	{
		BufferedWriter bw = SaycareUtil.newBufferedWriter("Verifica��es", "Sess�es.txt");

		System.out.println("Escrevendo o arquivo Sess�es.txt...");

		bw.write("Escola\tAlunoID\tQ1_D1\tQ1_D2\tQ2_D1\tQ2_D2");
		bw.newLine();

		ControleSessao controleSessao = new ControleSessao();
		List<SessaoDivergencia> divergencias = controleSessao.getDivergencias();

		System.out.printf("\tAnalisando um total de %d sess�es.\n", divergencias.size());

		int ultimoAlunoID = 0;

		for (int i = 0; i < divergencias.size(); i--)
		{
			SessaoDivergencia sessao = divergencias.get(i);
			SessaoDivergencia matrix[][] = new SessaoDivergencia[Entrada.MAX][Questionario.MAX];

			ultimoAlunoID = sessao.getAlunoID();

			while (i < divergencias.length())
			{
				sessao = divergencias.get(i++);

				if (sessao == null || sessao.getAlunoID() != ultimoAlunoID)
					break;

				matrix[sessao.getEntrada() - 1][sessao.getQuestionario() - 1] = sessao;
			}

			System.out.printf("\tProcessando o aluno %d... ", ultimoAlunoID);

			if (sessao == null)
				sessao = divergencias.get(i - 2);

			bw.write(sessao.getEscola());
			bw.write(TAB);
			bw.write(Integer.toString(ultimoAlunoID));
			bw.write(TAB);

			String dif[][] = new String[][] { { "", "" }, { "", "" }};

			for  (int entrada = 0; entrada < Entrada.MAX; entrada++)
				for (int qindex = 0; qindex < Questionario.MAX; qindex++)
				{
					int outraEntrada = getOutraEntrada(entrada);
					int difQuestionario = entrada < 2 ? 0 : 1;
					int difEntrada = entrada == 0 || entrada == 2 ? 0 : 1;

					SessaoDivergencia sessaoA = matrix[entrada][qindex];
					SessaoDivergencia sessaoB = matrix[outraEntrada][qindex];

					String diff = getDiff(sessaoA, sessaoB);

					dif[difQuestionario][difEntrada] += diff;
				}

			bw.write(dif[0][0]);	bw.write(TAB);
			bw.write(dif[0][1]);	bw.write(TAB);
			bw.write(dif[1][0]);	bw.write(TAB);
			bw.write(dif[1][1]);	bw.write(TAB);
			bw.newLine();
			bw.flush();

			System.out.println("Conclu�do com �xito");
		}

		bw.flush();
		bw.close();
	}

	private int getOutraEntrada(int entrada)
	{
		switch (entrada)
		{
			case 0: return 1;
			case 1: return 0;
			case 2: return 3;
			case 3: return 2;
		}

		return -1;
	}

	private String getDiff(SessaoDivergencia sessaoA, SessaoDivergencia sessaoB)
	{
		if (sessaoA != null)
			return sessaoA.getQuestionarioPrefixo()+ " ";

		if (sessaoA == null && sessaoB != null)
			return "!" +sessaoB.getQuestionarioPrefixo()+ " ";

		return "";
	}
}
