package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;
import java.io.IOException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.lang.FloatUtil;

import tk.saycare.controles.ControleResposta;
import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Respostas;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.util.MapSessaoAluno;
import tk.saycare.util.SaycareUtil;

public class VerificarRespostas2 implements ICall
{
	private static final int VR_INCOMPLETO = 0;
	private static final int VR_DIVERGENTE = 1;
	private static final int VR_INTERPRETATIVO = 2;
	private static final int VR_VALIDO = 3;
	private static final int VR_MAX = 4;
	private static final String CIDADES[] = new String[]
			{ "S�o Paulo", "Teresina", "Buenos Aires", "Medell�n", "Lima", "Montevideo", "Santiago" };

	private Map<Integer, Aluno> alunos;
	private Map<Integer, Questionario> questionarios;
	private Map<SessaoKey, Sessao> sessoes;
	private MapSessaoAluno respostas;

	public Map<Integer, Aluno> getAlunos()
	{
		return alunos;
	}

	public void setAlunos(Map<Integer, Aluno> alunos)
	{
		this.alunos = alunos;
	}

	public Map<Integer, Questionario> getQuestionarios()
	{
		return questionarios;
	}

	public void setQuestionarios(Map<Integer, Questionario> questionarios)
	{
		this.questionarios = questionarios;
	}

	public Map<SessaoKey, Sessao> getSessoes()
	{
		return sessoes;
	}

	public void setSessoes(Map<SessaoKey, Sessao> sessoes)
	{
		this.sessoes = sessoes;
	}

	public MapSessaoAluno getRespostas()
	{
		return respostas;
	}

	public void setRespostas(MapSessaoAluno respostas)
	{
		this.respostas = respostas;
	}

	@Override
	public void call() throws Exception
	{
		ControleResposta controleResposta = new ControleResposta();
		respostas = controleResposta.mapear(sessoes);

		BufferedWriter bws[][] = new BufferedWriter[questionarios.size()][CIDADES.length];

		this.questionarios.setGeneric(Questionario.class);
		Questionario questionarios[] = this.questionarios.toArray();

		int validos = 0;
		int divergentes = 0;
		int interpretativos = 0;

		for (int i = 0; i < CIDADES.length; i++)
			for (int j = 0; j < bws.length; j++)
				bws[j][i] = SaycareUtil.newBufferedWriter("Verifica��es", CIDADES[i]+ "_" +questionarios[j].getNome()+ ".txt");

		for (Aluno aluno : alunos)
		{
			for (Questionario questionario : questionarios)
			{
				Sessao entradas[] = respostas.getSessao(aluno, questionario);

				if (entradas != null && entradas[0] != null && entradas[1] != null)
				{
					BufferedWriter bw[] = bws[questionario.getId() - 1];
					int validacao[] = callVerificarRespostasDe(bw, entradas[0], entradas[1]);

					validos += validacao[VR_VALIDO];
					divergentes += validacao[VR_DIVERGENTE];
					interpretativos += validacao[VR_INTERPRETATIVO];
				}
			}

			System.out.printf("Diverg�ncias do aluno %d exportadas.\n", aluno.getId());
		}

		int total = validos + divergentes + interpretativos;
		int validosParcial = validos + interpretativos;

		System.out.println("Diverg�ncias conclu�das:");
		System.out.printf("\t- Validados: [%d de %d (%.2f%%)] total\n", validos, total, FloatUtil.toPorcent(validos, total));
		System.out.printf("\t- Divergentes: [%d de %d (%.2f%%)] total ou [%d de %d (%.2f%%)] parcial\n", divergentes, total, FloatUtil.toPorcent(divergentes, total), divergentes, validosParcial, FloatUtil.toPorcent(divergentes, validosParcial));
		System.out.printf("\t- Interpretativos: [%d de %d (%.2f%%)] total ou [%d de %d (%.2f%%)] parcial\n", interpretativos, total, FloatUtil.toPorcent(interpretativos, total), interpretativos, validos, FloatUtil.toPorcent(interpretativos, validos));
	}

	private int[] callVerificarRespostasDe(BufferedWriter bws[], Sessao sessaoA, Sessao sessaoB) throws IOException
	{
		int validados[] = new int[VR_MAX];

		Respostas respostasA = sessaoA.getRespostas();
		Respostas respostasB = sessaoB.getRespostas();

		int alunoID = sessaoA.getKey().getAluno().getId();
		String questionario = sessaoA.getKey().getQuestionario().getNome();

		BufferedWriter bw = bwCidade(sessaoA, bws);

		bw.write(String.format("- [AlunoID: %7d][Question�rio: %s]", alunoID, questionario));
		bw.newLine();

		if (respostasA == null && respostasB == null)
			bw.write("Respostas da primeira e segunda digita��o n�o encontrado.\n");

		else if (respostasA == null)
			bw.write("Respostas da primeira digita��o n�o encontrado.\n");

		else if (respostasB == null)
			bw.write("Respostas da segunda digita��o n�o encontrado.\n");

		else
		{
			bw.newLine();

			for (int pergunta = 1; pergunta <= respostasA.getPerguntasCount(); pergunta++)
				for (int ordem = 1; ordem <= respostasA.getPerguntaOrdens(pergunta); ordem++)
				{
					String a = respostasA.getResposta(pergunta, ordem);
					String b = respostasB.getResposta(pergunta, ordem);

					if (a != null && a.isEmpty())	a = ".";
					if (b != null && b.isEmpty())	b = ".";

					if (a == null || b == null)
						validados[VR_INCOMPLETO]++;

					else if ((a.equals("0") && b.equals(".")) || (a.equals(".") && b.equals("0")))
					{
						bw.write(String.format("P%d_O%d: [%s] x [%s]\n", pergunta, ordem, a, b));
						validados[VR_INTERPRETATIVO]++;
					}

					else if (!a.equalsIgnoreCase(b))
					{
						bw.write(String.format("P%d_O%d: [%s] x [%s]\n", pergunta, ordem, a, b));
						validados[VR_DIVERGENTE]++;
					}

					else
						validados[VR_VALIDO]++;
				}
		}

		bw.newLine();
		bw.flush();

		return validados;
	}

	private BufferedWriter bwCidade(Sessao sessaoA, BufferedWriter[] bws)
	{
		int id = sessaoA.getKey().getAluno().getId();

		if (id < 1999999)
			return bws[0];

		if (id < 2999999)
			return bws[1];

		if (id < 3999999)
			return bws[2];

		if (id < 4999999)
			return bws[3];

		if (id < 5999999)
			return bws[4];

		if (id < 6999999)
			return bws[5];

		if (id < 7999999)
			return bws[6];

		return null;
	}
}
