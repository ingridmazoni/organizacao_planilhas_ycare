package tk.saycare.fronteira.exportacao;

import org.diverproject.util.lang.IntUtil;

import tk.saycare.entidades.Entrada;
import tk.saycare.entidades.Questionario;
import tk.saycare.util.FaixaEtaria;

public class ContagemSessoes
{
	private int counts[][][];

	public ContagemSessoes()
	{
		counts = new int[Questionario.MAX][Entrada.MAX][3];
	}

	public void set(int questionario, int entrada, int faixaEtaria, int quantidade)
	{
		if (IntUtil.interval(--questionario, 0, counts.length))
			if (IntUtil.interval(--entrada, 0, counts[questionario].length))
				if (IntUtil.interval(faixaEtaria, 0, counts[questionario][entrada].length))
					counts[questionario][entrada][faixaEtaria] = quantidade;
	}

	public int get(Questionario questionario, int entrada, FaixaEtaria faixaEtaria)
	{
		if (!IntUtil.interval(questionario.getId(), 0, counts.length))
			if (!IntUtil.interval(faixaEtaria.ordinal(), 0, 3))
				return -1;

		int qid = questionario.getId() - 1;
		int entrada1 = entrada == 1 ? 0 : 2;
		int entrada2 = entrada == 1 ? 1 : 3;

		int count1 = counts[qid][entrada1][faixaEtaria.ordinal()];
		int count2 = counts[qid][entrada2][faixaEtaria.ordinal()];

		return IntUtil.major(count1, count2);
	}
}
