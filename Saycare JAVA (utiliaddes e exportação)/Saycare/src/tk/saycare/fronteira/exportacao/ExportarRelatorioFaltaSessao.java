package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;

import org.diverproject.util.collection.Map;

import tk.saycare.controles.ControleSessao;
import tk.saycare.entidades.Questionario;
import tk.saycare.util.FaixaEtaria;
import tk.saycare.util.SaycareUtil;

public class ExportarRelatorioFaltaSessao implements ICall
{
	private Map<Integer, Questionario> questionarios;

	public Map<Integer, Questionario> getQuestionarios()
	{
		return questionarios;
	}

	public void setQuestionarios(Map<Integer, Questionario> questionarios)
	{
		this.questionarios = questionarios;
	}

	@Override
	public void call() throws Exception
	{
		BufferedWriter bw = SaycareUtil.newBufferedWriter("Relat�rios", "Sess�es Faltando.txt");

		writeCidade(bw, "S�o Paulo", 1000000, 1999999);
		writeCidade(bw, "Teresina", 2000000, 2999999);
		writeCidade(bw, "Buenos Aires", 3000000, 3999999);
		writeCidade(bw, "Medell�n", 4000000, 4999999);
		writeCidade(bw, "Lima", 5000000, 5999999);
		writeCidade(bw, "Montevideo", 6000000, 6999999);
		writeCidade(bw, "Santiago", 7000000, 7999999);

		bw.flush();
		bw.close();
	}

	private void writeCidade(BufferedWriter bw, String cidade, int minAlunoID, int maxAlunoID) throws IOException, SQLException
	{
		ControleSessao controleSessao = new ControleSessao();
		ContagemSessoes contagem = controleSessao.contar(minAlunoID, maxAlunoID);

		bw.write(String.format("Formato: [Pre-Escolar][Escolar][Adolescente]", cidade));
		bw.newLine();
		bw.write(String.format("-- Cidade: %s", cidade));
		bw.newLine();

		for (Questionario questionario : questionarios)
		{
			int ea1 = contagem.get(questionario, 1, FaixaEtaria.ADOLESCENTE);
			int ea2 = contagem.get(questionario, 2, FaixaEtaria.ADOLESCENTE);
			int ee1 = contagem.get(questionario, 1, FaixaEtaria.ESCOLAR);
			int ee2 = contagem.get(questionario, 2, FaixaEtaria.ESCOLAR);
			int ep1 = contagem.get(questionario, 1, FaixaEtaria.PRE_ESCOLAR);
			int ep2 = contagem.get(questionario, 2, FaixaEtaria.PRE_ESCOLAR);

			bw.write(String.format("%4s: ", questionario.getPrefixo()));
			bw.write(String.format("Q1 [%3d/%3d/%3d]", ep1, ee1, ea1));
			bw.write(" - ");
			bw.write(String.format("Q2 [%3d/%3d/%3d]", ep2, ee2, ea2));
			bw.newLine();
		}

		bw.newLine();
		bw.flush();
	}
}
