package tk.saycare.fronteira.exportacao;

public class Label
{
	private String name;
	private int pergunta;
	private int ordem;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getPergunta()
	{
		return pergunta;
	}

	public void setPergunta(int questao)
	{
		this.pergunta = questao;
	}

	public int getOrdem()
	{
		return ordem;
	}

	public void setOrdem(int ordem)
	{
		this.ordem = ordem;
	}
}
