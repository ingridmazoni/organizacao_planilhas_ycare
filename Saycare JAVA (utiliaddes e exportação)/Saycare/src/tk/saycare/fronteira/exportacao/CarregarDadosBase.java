package tk.saycare.fronteira.exportacao;

import java.sql.SQLException;

import org.diverproject.util.collection.Map;

import tk.saycare.controles.ControleAluno;
import tk.saycare.controles.ControleEscola;
import tk.saycare.controles.ControleQuestionario;
import tk.saycare.controles.ControleResposta;
import tk.saycare.controles.ControleSessao;
import tk.saycare.controles.ControleUsuario;
import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Escola;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.entidades.Usuario;

public class CarregarDadosBase implements ICall
{
	private Map<Integer, Escola> escolas;
	private Map<Integer, Aluno> alunos;
	private Map<Integer, Questionario> questionarios;
	private Map<Integer, Usuario> usuarios;
	private Map<SessaoKey, Sessao> sessoes;

	public Map<Integer, Escola> getEscolas()
	{
		return escolas;
	}

	public Map<Integer, Aluno> getAlunos()
	{
		return alunos;
	}

	public Map<Integer, Questionario> getQuestionarios()
	{
		return questionarios;
	}

	public Map<Integer, Usuario> getUsuarios()
	{
		return usuarios;
	}

	public Map<SessaoKey, Sessao> getSessoes()
	{
		return sessoes;
	}

	@Override
	public void call() throws Exception
	{
		callCarregarEscolas();
		callCarregarAlunos();
		callCarregarQuestioarnios();
		callCarregarUsuarios();
		callCarregarSessoes();
		callCarregarRespostas();
	}

	private void callCarregarEscolas() throws SQLException
	{
		ControleEscola controleEscola = new ControleEscola();

		if (escolas != null)
			escolas.clear();

		escolas = controleEscola.listarTodos();

		System.out.printf("%d escolas carregadas.\n", escolas.size());
	}

	private void callCarregarAlunos() throws SQLException
	{
		ControleAluno controleAluno = new ControleAluno();

		if (alunos != null)
			alunos.clear();

		alunos = controleAluno.listarTodos();

		for (Aluno aluno : alunos)
		{
			Escola escola = escolas.get(aluno.getEscola().getId());
			aluno.setEscola(escola);
		}

		System.out.printf("%d alunos carregados.\n", alunos.size());
	}

	private void callCarregarQuestioarnios() throws SQLException
	{
		ControleQuestionario controleQuestionario = new ControleQuestionario();

		if (questionarios != null)
			questionarios.clear();

		questionarios = controleQuestionario.listarTodos();

		System.out.printf("%d questionarios carregados.\n", questionarios.size());
	}

	private void callCarregarUsuarios() throws SQLException
	{
		ControleUsuario controleUsiario = new ControleUsuario();

		if (usuarios != null)
			usuarios.clear();

		usuarios = controleUsiario.listarTodos();

		System.out.printf("%d usuarios carregados.\n", usuarios.size());
	}

	private void callCarregarSessoes() throws SQLException
	{
		ControleSessao controleSessao = new ControleSessao();

		if (sessoes != null)
			sessoes.clear();

		sessoes = controleSessao.listarTodos();
		controleSessao.atualizarReferencias(sessoes, usuarios, alunos, questionarios);

		System.out.printf("%d sess�es carregadas.\n", sessoes.size());
	}

	private void callCarregarRespostas() throws SQLException
	{
		ControleResposta controleResposta = new ControleResposta();
		controleResposta.calcularEspaco();

		System.out.printf("Espa�o para respostas calculadas\n");

		int total = controleResposta.carregar(sessoes);

		System.out.printf("%d respostas carregadas\n", total);
	}
}
