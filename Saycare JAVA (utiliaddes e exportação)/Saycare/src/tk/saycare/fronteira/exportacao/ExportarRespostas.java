package tk.saycare.fronteira.exportacao;

import java.io.BufferedWriter;
import java.io.IOException;

import org.diverproject.util.DateUtil;
import org.diverproject.util.collection.Map;

import tk.saycare.controles.ControleResposta;
import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Respostas;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.util.MapSessaoAluno;
import tk.saycare.util.SaycareUtil;

public class ExportarRespostas implements ICall
{
	private Map<Integer, Aluno> alunos;
	private Map<Integer, Questionario> questionarios;
	private Map<SessaoKey, Sessao> sessoes;
	private MapSessaoAluno respostas;

	public void setAlunos(Map<Integer, Aluno> alunos)
	{
		this.alunos = alunos;
	}

	public void setQuestionarios(Map<Integer, Questionario> questionarios)
	{
		this.questionarios = questionarios;
	}

	public void setSessoes(Map<SessaoKey, Sessao> sessoes)
	{
		this.sessoes = sessoes;
	}

	public MapSessaoAluno getRespostas()
	{
		return respostas;
	}

	@Override
	public void call() throws Exception
	{
		ControleResposta controleResposta = new ControleResposta();
		respostas = controleResposta.mapear(sessoes);

		BufferedWriter bws[] = new BufferedWriter[questionarios.size()];

		this.questionarios.setGeneric(Questionario.class);
		Questionario questionarios[] = this.questionarios.toArray();

		for (int i = 0; i < bws.length; i++)
			bws[i] = newExportarRespostas(questionarios[i]);

		for (Aluno aluno : alunos)
			for (Questionario questionario : questionarios)
			{
				BufferedWriter bw = bws[questionario.getId() - 1];

				Sessao entradas[] = new Sessao[]
				{
					respostas.getSessao(aluno, questionario, 1),
					respostas.getSessao(aluno, questionario, 2),
					respostas.getSessao(aluno, questionario, 3),
					respostas.getSessao(aluno, questionario, 4)
				};

				System.out.printf("\tExportando aluno '%d' (%s)... ", aluno.getId(), questionario.getNome());

				bw.write(Integer.toString(aluno.getId()));
				bw.write("\t");
				bw.write(aluno.getFaixaEtaria().VALUE);
				bw.write("\t");
				bw.write(aluno.getEscola().getTipoString());
				bw.write("\t");
				bw.flush();

				if (entradas[0] != null)
					callEscreverRespostas(bw, entradas[0], entradas[1], questionario.getId());

				if (entradas[2] != null)
					callEscreverRespostas(bw, entradas[2], entradas[3], questionario.getId());

				bw.newLine();
				bw.flush();

				System.out.printf(" conclu�do!\n");
			}

		for (int i = 0; i < bws.length; i++)
		{
			System.out.printf("'%s.xls' exportItemado com �xtio!\n", questionarios[i].getNome());
			bws[i].close();
		}
	}

	public boolean hasSessao(Sessao[] sessoes)
	{
		return	(sessoes[0] != null) ||
				(sessoes[2] != null);
	}

	private BufferedWriter newExportarRespostas(Questionario questionario) throws IOException
	{
		BufferedWriter bw = SaycareUtil.newBufferedWriter("Exporta��o", questionario.getNome()+ ".xls");

		ControleResposta controleRespostas = new ControleResposta();

		bw.write("AlunoID\tFaixa\tEscola\t");

		for (int entrada = 1; entrada <= 2; entrada++)
		{
			bw.write(String.format("%s_E%d\t", questionario.getPrefixo(), entrada));

			for (int pergunta = 1; pergunta <= controleRespostas.getPerguntasCount(questionario.getId()); pergunta++)
				for (int ordem = 1; ordem <= controleRespostas.getPerguntaOrdens(pergunta, questionario.getId()); ordem++)
				{
					bw.write(String.format("%s_%d_%d_Q%d", questionario.getPrefixo(), pergunta, ordem, entrada));
					bw.write("\t");
				}
		}

		bw.newLine();
		bw.flush();

		return bw;
	}

	private void callEscreverRespostas(BufferedWriter bw, Sessao sessaoA, Sessao sessaoB, int questionario) throws IOException
	{
		if (sessaoA.getRespostas() == null)
			return;

		if (sessaoA.getDate() != null)
		{
			Respostas respostasA = sessaoA.getRespostas();

			bw.write(DateUtil.toString(sessaoA.getDate()));
			bw.write("\t");

			for (int pergunta = 1; pergunta <= respostasA.getPerguntasCount(); pergunta++)
				for (int ordem = 1; ordem <= respostasA.getPerguntaOrdens(pergunta); ordem++)
				{
					String resposta = respostasA.getResposta(pergunta, ordem);

					if (resposta != null && resposta.equals("01/01/0001"))
						resposta = ".";

					if (resposta != null)
					{
						if (resposta.isEmpty())
							bw.write("0");
						else
							bw.write(resposta.toUpperCase());
					}
					else
						bw.write(".");

					bw.write("\t");
				}
		}

		bw.flush();
	}
}
