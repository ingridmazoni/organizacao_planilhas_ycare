package tk.saycare.fronteira.exportacao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.diverproject.util.lang.IntUtil;

public class ExportarColunasEmLabels implements ICall
{
	@Override
	public void call() throws Exception
	{
		FileReader in = new FileReader("labels_dg.txt");
		BufferedReader reader = new BufferedReader(in);

		LabelList listAdolescentes = new LabelList();
		LabelList listEscolares = new LabelList();

		reader.readLine();

		while (reader.ready())
		{
			String columns[] = reader.readLine().split("\t");

			String label = columns[0];

			if (columns.length > 2 && !columns[2].isEmpty())
			{
				String idAdolescente[] = columns[2].split("_");

				int perguntaAdolescente = IntUtil.parse(idAdolescente[0].substring(1, idAdolescente[0].length()));
				int ordemAdolescente = IntUtil.parse(idAdolescente[1].substring(1, idAdolescente[1].length()));

				Label adolescente = new Label();
				adolescente.setName(label);
				adolescente.setOrdem(ordemAdolescente);
				adolescente.setPergunta(perguntaAdolescente);
				listAdolescentes.add(adolescente);
			}

			if (columns.length > 3 && !columns[3].isEmpty())
			{
				String idEscolar[] = columns[3].split("_");

				int perguntaEscolar = IntUtil.parse(idEscolar[0].substring(1, idEscolar[0].length()));
				int ordemEscolar = IntUtil.parse(idEscolar[1].substring(1, idEscolar[1].length()));

				Label escolar = new Label();
				escolar.setName(label);
				escolar.setOrdem(ordemEscolar);
				escolar.setPergunta(perguntaEscolar);
				listEscolares.add(escolar);
			}
		}

		reader.close();

		FileWriter out = new FileWriter("colunas_dg.txt");
		BufferedWriter writer = new BufferedWriter(out);

		escreverColunas("colunas_dg_adolescente.txt", "DG", listAdolescentes, writer);
		escreverColunas("colunas_dg_escolar.txt", "DG", listEscolares, writer);

		writer.close();
	}

	private void escreverColunas(String base, String prefixo, LabelList labels, BufferedWriter writer) throws IOException
	{
		FileReader in = new FileReader(base);
		BufferedReader reader = new BufferedReader(in);

		String columns[] = reader.readLine().split("\t");
		reader.close();

		for (String column : columns)
		{
			String parts[] = column.split("_");

			if (parts.length == 4)
			{
				String sPergunta = parts[1];
				String sOrdem = parts[2];
				String sEntrada = parts[3];

				int pergunta = IntUtil.parse(sPergunta);
				int ordem = IntUtil.parse(sOrdem);
				int entrada = IntUtil.parse(sEntrada.substring(1, sEntrada.length()));

				Label label = labelOf(labels, pergunta, ordem);

				if (label != null)
					writer.write(String.format("%s_E%d", label.getName(), entrada));
				else
					writer.write(column);

				writer.write("\t");
			}

			else
			{
				writer.write(column);
				writer.write("\t");
			}
		}

		writer.newLine();
	}

	private Label labelOf(LabelList labels, int pergunta, int ordem)
	{
		for (Label label : labels)
			if (label.getPergunta() == pergunta && label.getOrdem() == ordem)
				return label;

		return null;
	}
}
