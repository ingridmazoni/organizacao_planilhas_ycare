package tk.saycare.entidades;

import java.util.Date;

import org.diverproject.util.DateUtil;
import org.diverproject.util.ObjectDescription;

public class Sessao
{
	private SessaoKey key;
	private int parte;
	private Date date;
	private Respostas respostas;

	public Sessao()
	{
		key = new SessaoKey();
	}

	public SessaoKey getKey()
	{
		return key;
	}

	public void isKey(SessaoKey key)
	{
		this.key.equals(key);
	}

	public int getParte()
	{
		return parte;
	}

	public void setParte(int parte)
	{
		this.parte = parte;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public Respostas getRespostas()
	{
		return respostas;
	}

	public void setRespostas(Respostas respostas)
	{
		this.respostas = respostas;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		key.toString(description);

		description.append("parte", parte);
		description.append("date", DateUtil.toString(date));

		return description.toString();
	}
}
