package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;

public class Questionario
{
	public static final int MAX = 12;
	private int id;
	private String nome;
	private String prefixo;

	public int getId()
	{
		return id;
	}

	public Questionario setId(int id)
	{
		this.id = id;
		return this;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public String getPrefixo()
	{
		return prefixo;
	}

	public Questionario setPrefixo(String prefixo)
	{
		this.prefixo = prefixo;
		return this;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Questionario)
		{
			Questionario questionario = (Questionario) obj;

			return questionario.id == id;
		}

		return false;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		description.append("id", id);
		description.append("nome", nome);
		description.append("prefixo", prefixo);

		return description.toString();
	}
}
