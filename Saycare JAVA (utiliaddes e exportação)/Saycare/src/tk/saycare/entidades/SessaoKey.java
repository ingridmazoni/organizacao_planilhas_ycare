package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;

public class SessaoKey
{
	private Usuario usuario;
	private Aluno aluno;
	private int entrada;
	private Questionario questionario;

	public Usuario getUsuario()
	{
		return usuario;
	}

	public void setUsuario(Usuario usuario)
	{
		this.usuario = usuario;
	}

	public Aluno getAluno()
	{
		return aluno;
	}

	public void setAluno(Aluno aluno)
	{
		this.aluno = aluno;
	}

	public int getEntrada()
	{
		return entrada;
	}

	public void setEntrada(int entrada)
	{
		this.entrada = entrada;
	}

	public Questionario getQuestionario()
	{
		return questionario;
	}

	public void setQuestionario(Questionario questionario)
	{
		this.questionario = questionario;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof SessaoKey)
		{
			SessaoKey key = (SessaoKey) obj;

			return	key.usuario.getId() == usuario.getId() &&
					key.aluno.getId() == aluno.getId() &&
					key.entrada == entrada &&
					key.questionario.equals(questionario);
		}

		return false;
	}

	public void toString(ObjectDescription description)
	{
		description.append("usuarioID", usuario == null ? "undefined" : usuario.getId());
		description.append("alunoID", aluno == null ? "undefined" : aluno);
		description.append("entrada", entrada);
		description.append("questionarioID", questionario == null ? "undefined" : questionario.getId());
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		toString(description);

		return description.toString();
	}
}
