package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;

public class SessaoDivergencia
{
	private int alunoID;
	private int entrada;
	private int questionario;
	private String questionarioPrefixo;
	private String escola;

	public int getAlunoID()
	{
		return alunoID;
	}

	public void setAlunoID(int alunoID)
	{
		this.alunoID = alunoID;
	}

	public int getEntrada()
	{
		return entrada;
	}

	public void setEntrada(int entrada)
	{
		this.entrada = entrada;
	}

	public int getQuestionario()
	{
		return questionario;
	}

	public void setQuestionario(int questionario)
	{
		this.questionario = questionario;
	}

	public String getQuestionarioPrefixo()
	{
		return questionarioPrefixo;
	}

	public void setQuestionarioPrefixo(String questionarioPrefixo)
	{
		this.questionarioPrefixo = questionarioPrefixo;
	}

	public String getEscola()
	{
		return escola;
	}

	public void setEscola(String escola)
	{
		this.escola = escola;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		description.append("alunoID", alunoID);
		description.append("questionarioPrefixo", questionarioPrefixo);
		description.append("entrada", entrada);

		return description.toString();
	}
}
