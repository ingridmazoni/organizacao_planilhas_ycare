package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;
import org.diverproject.util.lang.IntUtil;

public class Entrada
{
	public static final int MAX = 4;
	private Sessao sessoes[][];

	public Entrada()
	{
		sessoes = new Sessao[MAX][Questionario.MAX];
	}

	public Sessao getSessao(int entrada, Questionario questionario)
	{
		if (valid(entrada, questionario))
			return sessoes[entrada - 1][questionario.getId() - 1];

		return null;
	}

	public void setSessao(int entrada, Sessao sessao)
	{
		Questionario questionario = sessao.getKey().getQuestionario();

		if (valid(entrada, questionario))
			sessoes[entrada - 1][questionario.getId() - 1] = sessao;
	}

	public boolean hasSessao(int entrada, Questionario questionario)
	{
		if (valid(entrada, questionario))
			return sessoes[entrada - 1][questionario.getId() - 1] != null;

		return false;
	}

	private boolean valid(int entrada, Questionario questionario)
	{
		if (IntUtil.interval(entrada, 1, MAX))
			if (IntUtil.interval(questionario.getId(), 1, Questionario.MAX))
				return true;

		return false;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		description.append("quantidades", sessoes.length);
		description.append("questionarios", sessoes[0].length);

		return description.toString();
	}
}
