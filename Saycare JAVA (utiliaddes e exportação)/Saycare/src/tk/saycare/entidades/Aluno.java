package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;

import tk.saycare.util.FaixaEtaria;
import tk.saycare.util.Sexo;

public class Aluno
{
	private int id;
	private Escola escola;
	private FaixaEtaria faixaEtaria;
	private String iniciais;
	private Sexo sexo;

	public int getId()
	{
		return id;
	}

	public Aluno setId(int id)
	{
		this.id = id;
		return this;
	}

	public Escola getEscola()
	{
		return escola;
	}

	public void setEscola(Escola escola)
	{
		this.escola = escola;
	}

	public FaixaEtaria getFaixaEtaria()
	{
		return faixaEtaria;
	}

	public void setFaixaEtaria(FaixaEtaria faixaEtaria)
	{
		this.faixaEtaria = faixaEtaria;
	}

	public String getIniciais()
	{
		return iniciais;
	}

	public void setIniciais(String iniciais)
	{
		this.iniciais = iniciais;
	}

	public Sexo getSexo()
	{
		return sexo;
	}

	public void setSexo(Sexo sexo)
	{
		this.sexo = sexo;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Aluno)
		{
			Aluno aluno = (Aluno) obj;

			return aluno.id == id;
		}

		return false;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		description.append("id", id);
		description.append("escola", escola == null ? "undefined" : escola.getNome());
		description.append("faixaEtaria", faixaEtaria);
		description.append("iniciais", iniciais);
		description.append("sexo", sexo);

		return description.toString();
	}
}
