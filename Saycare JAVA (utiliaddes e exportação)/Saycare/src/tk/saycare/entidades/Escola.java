package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;

public class Escola
{
	private int id;
	private String nome;
	private int tipo;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public int getTipo()
	{
		return tipo;
	}

	public String getTipoString()
	{
		switch (tipo)
		{
			case 1: return "PUB";
			case 2: return "PRI";
		}

		return "null";
	}

	public void setTipo(int tipo)
	{
		this.tipo = tipo;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		description.append("id", id);
		description.append("nome", nome);
		description.append("tipo", tipo);

		return description.toString();
	}
}
