package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;

import tk.saycare.util.Sexo;

public class Usuario
{
	private int id;
	private String usuario;
	private String senha;
	private String nome;
	private String ultimoNome;
	private Sexo sexo;
	private int acesso;
	private String email;
	private String funcao;

	public int getId()
	{
		return id;
	}

	public Usuario setId(int id)
	{
		this.id = id;
		return this;
	}

	public String getUsuario()
	{
		return usuario;
	}

	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}

	public String getSenha()
	{
		return senha;
	}

	public void setSenha(String senha)
	{
		this.senha = senha;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public String getUltimoNome()
	{
		return ultimoNome;
	}

	public void setUltimoNome(String ultimoNome)
	{
		this.ultimoNome = ultimoNome;
	}

	public Sexo getSexo()
	{
		return sexo;
	}

	public void setSexo(Sexo sexo)
	{
		this.sexo = sexo;
	}

	public int getAcesso()
	{
		return acesso;
	}

	public void setAcesso(int acesso)
	{
		this.acesso = acesso;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFuncao()
	{
		return funcao;
	}

	public void setFuncao(String funcao)
	{
		this.funcao = funcao;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Usuario)
		{
			Usuario usuario = (Usuario) obj;

			return usuario.id == id;
		}

		return false;
	}

	@Override
	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		description.append("id", id);
		description.append("usuario", usuario);
		description.append("senha", senha);
		description.append("nome", nome);
		description.append("ultimo_nome", ultimoNome);
		description.append("sexo", sexo);
		description.append("acesso", acesso);
		description.append("email", email);
		description.append("funcao", funcao);

		return description.toString();
	}
}
