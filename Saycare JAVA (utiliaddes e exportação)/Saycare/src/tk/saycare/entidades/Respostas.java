package tk.saycare.entidades;

import org.diverproject.util.ObjectDescription;
import org.diverproject.util.lang.IntUtil;

public class Respostas
{
	private String respostas[][];

	public void setPerguntas(int quantidade)
	{
		respostas = new String[quantidade][];
	}

	public void setPerguntaOrdens(int pergunta, int ordens)
	{
		if (IntUtil.interval(pergunta, 1, respostas.length))
			respostas[pergunta - 1] = new String[ordens];
	}

	public void setResposta(int pergunta, int ordem, String resposta)
	{
		if (IntUtil.interval(pergunta, 1, respostas.length))
			if (IntUtil.interval(ordem, 1, respostas[pergunta - 1].length))
				respostas[pergunta - 1][ordem - 1] = resposta;
	}

	public String getResposta(int pergunta, int ordem)
	{
		if (IntUtil.interval(pergunta, 1, respostas.length))
			if (IntUtil.interval(ordem, 1, respostas[pergunta - 1].length))
				return respostas[pergunta - 1][ordem - 1];

		return null;
	}

	public int getPerguntasCount()
	{
		return respostas.length;
	}

	public int getPerguntaOrdens(int pergunta)
	{
		if (IntUtil.interval(pergunta, 1, respostas.length))
			return respostas[pergunta - 1].length;

		return 0;
	}

	public String toString()
	{
		ObjectDescription description = new ObjectDescription(getClass());

		for (int i = 0; i < respostas.length; i++)
		{
			String temp = "";

			for (int j = 0; j < respostas[i].length; j++)
			{
				temp += respostas[i][j];

				if (j < respostas[i].length - 1)
					temp += " | ";
			}

			description.append("pergunta", i + 1);
			description.append("respostas", temp);
		}

		return description.toString();
	}
}
