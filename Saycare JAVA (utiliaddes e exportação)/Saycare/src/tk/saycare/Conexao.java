package tk.saycare;

import java.sql.Connection;

import org.diverproject.util.MessageUtil;
import org.diverproject.util.sql.MySQL;

public class Conexao
{
	private static MySQL mysqlOriginal;
	private static MySQL mysqlRelease;

	public static Connection getConnection()
	{
		return getConnection(true);
	}

	public static Connection getConnection(boolean release)
	{
		return release ? getConnectionRelease() : getConnectionOriginal();
	}

	private static Connection getConnectionRelease()
	{
		if (mysqlRelease == null)
		{
			mysqlRelease = new MySQL();
			mysqlRelease.setHost("143.107.75.35");
			mysqlRelease.setDatabase("saycar");
			mysqlRelease.setUsername("saycar");
			mysqlRelease.setPassword("saycar**");

			try {
				mysqlRelease.connect();
			} catch (Exception e) {
				MessageUtil.die(e);
			}
		}

		return mysqlRelease.getConnection();
	}

	private static Connection getConnectionOriginal()
	{
		if (mysqlOriginal == null)
		{
			mysqlOriginal = new MySQL();
			mysqlOriginal.setHost("143.107.75.35");
			mysqlOriginal.setDatabase("saycar");
			mysqlOriginal.setUsername("saycar");
			mysqlOriginal.setPassword("saycar**");

			try {
				mysqlOriginal.connect();
			} catch (Exception e) {
				MessageUtil.die(e);
			}
		}

		return mysqlOriginal.getConnection();
	}
}
