package tk.saycare.controles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.IntegerLargeMap;

import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Escola;
import tk.saycare.util.FaixaEtaria;
import tk.saycare.util.Sexo;

public class ControleAluno extends ControleAbstract
{
	public Map<Integer, Aluno> listarTodos() throws SQLException
	{
		String sql = "SELECT * FROM alunos WHERE id >= ? AND id <= ?";

		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, MIN_ALUNO_ID);
		ps.setInt(2, MAX_ALUNO_ID);

		ResultSet rs = ps.executeQuery();

		Map<Integer, Aluno> alunos = new IntegerLargeMap<Aluno>();

		while (rs.next())
		{
			Aluno aluno = criar(rs);
			alunos.add(aluno.getId(), aluno);
		}

		return alunos;
	}

	private Aluno criar(ResultSet rs) throws SQLException
	{
		Escola escola = new Escola();
		escola.setId(rs.getInt("escola"));

		Aluno aluno = new Aluno();
		aluno.setId(rs.getInt("id"));
		aluno.setFaixaEtaria(FaixaEtaria.parse(rs.getInt("faixa_etaria")));
		aluno.setSexo(Sexo.parse(rs.getString("sexo")));
		aluno.setIniciais(rs.getString("iniciais"));
		aluno.setEscola(escola);

		return aluno;
	}
}
