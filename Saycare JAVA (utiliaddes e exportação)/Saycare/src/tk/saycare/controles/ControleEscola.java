package tk.saycare.controles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;

import tk.saycare.entidades.Escola;

public class ControleEscola extends ControleAbstract
{
	public Map<Integer, Escola> listarTodos() throws SQLException
	{
		String sql = "SELECT * FROM escolas";

		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		Map<Integer, Escola> escolas = new IntegerLittleMap<Escola>();

		while (rs.next())
		{
			Escola escola = criar(rs);
			escolas.add(escola.getId(), escola);
		}

		return escolas;
	}

	private Escola criar(ResultSet rs) throws SQLException
	{
		Escola escola = new Escola();
		escola.setId(rs.getInt("id"));
		escola.setNome(rs.getString("nome"));
		escola.setTipo(rs.getInt("tipo"));

		return escola;
	}
}
