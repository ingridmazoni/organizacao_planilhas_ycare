package tk.saycare.controles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.diverproject.util.collection.List;
import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.DynamicList;

import tk.saycare.entidades.Aluno;
import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoDivergencia;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.entidades.Usuario;
import tk.saycare.fronteira.exportacao.ContagemSessoes;
import tk.saycare.util.MapSessao;

public class ControleSessao extends ControleAbstract
{
	public Map<SessaoKey, Sessao> listarTodos() throws SQLException
	{
		return listar(MIN_DATE, MIN_ALUNO_ID, MAX_ALUNO_ID);
	}

	public Map<SessaoKey, Sessao> listar(java.sql.Date maxDate, int minAlunoID, int maxAlunoID) throws SQLException
	{
		Map<SessaoKey, Sessao> sessoes = new MapSessao();

		String sql = "SELECT usuario, aluno, entrada, questionario, parte, data, faixa_etaria FROM sessoes "
					+"INNER JOIN alunos ON alunos.id = sessoes.aluno "
					+"WHERE aluno >= ? AND aluno <= ? AND (data >= ? AND data <= ?)";

		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, minAlunoID);
		ps.setInt(2, maxAlunoID);
		ps.setDate(3, MIN_DATE);
		ps.setDate(4, MAX_DATE);

		ResultSet rs = ps.executeQuery();

		while (rs.next())
		{
			Sessao sessao = criar(rs);
			sessoes.add(sessao.getKey(), sessao);
		}

		return sessoes;
	}

	public ContagemSessoes contar(int minAlunoID, int maxAlunoID) throws SQLException
	{
		String sql = "SELECT cidade, questionario, entrada, faixa_etaria, COUNT(*) as count FROM sessoes "
					+"INNER JOIN alunos ON alunos.id = sessoes.aluno "
					+"INNER JOIN cidades ON alunos.cidade = cidades.id "
					+"INNER JOIN questionarios ON sessoes.questionario = questionarios.id "
					+"WHERE alunos.id >= ? AND alunos.id <= ? AND (data >= ? AND data <= ?)"
					+"GROUP BY cidades.id, questionarios.id, entrada, faixa_etaria";

		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, minAlunoID);
		ps.setInt(2, maxAlunoID);
		ps.setDate(3, MIN_DATE);
		ps.setDate(4, MAX_DATE);

		ResultSet rs = ps.executeQuery();
		ContagemSessoes contagem = new ContagemSessoes();

		while (rs.next())
		{
			int questionario = rs.getInt("questionario");
			int entrada = rs.getInt("entrada");
			int faixaEtaria = rs.getInt("faixa_etaria");
			int quantidade = rs.getInt("count");

			contagem.set(questionario, entrada, faixaEtaria, quantidade);
		}

		return contagem;
	}

	public List<SessaoDivergencia> getDivergencias() throws SQLException
	{
		String sql = "SELECT aluno, questionario, questionarios.prefixo, entrada, escolas.nome FROM sessoes "
					+"INNER JOIN questionarios ON questionarios.id = sessoes.questionario "
					+"INNER JOIN alunos ON alunos.id = sessoes.aluno "
					+"INNER JOIN escolas ON escolas.id = alunos.escola "
					+"ORDER BY aluno, entrada, questionario";

		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		List<SessaoDivergencia> lista = new DynamicList<>();

		while (rs.next())
		{
			SessaoDivergencia sessaoDivergencia = criarSessaoDivergencia(rs);
			lista.add(sessaoDivergencia);
		}

		return lista;
	}

	private Sessao criar(ResultSet rs) throws SQLException
	{
		Sessao sessao = new Sessao();

		sessao.getKey().setUsuario(new Usuario().setId(rs.getInt("usuario")));
		sessao.getKey().setAluno(new Aluno().setId(rs.getInt("aluno")));
		sessao.getKey().setEntrada(rs.getInt("entrada"));
		sessao.getKey().setQuestionario(new Questionario().setId(rs.getInt("questionario")));

		sessao.setDate(new Date(rs.getDate("data").getTime()));
		sessao.setParte(rs.getInt("parte"));

		return sessao;
	}

	private SessaoDivergencia criarSessaoDivergencia(ResultSet rs) throws SQLException
	{
		SessaoDivergencia sessao = new SessaoDivergencia();
		sessao.setAlunoID(rs.getInt("aluno"));
		sessao.setEntrada(rs.getInt("entrada"));
		sessao.setQuestionario(rs.getInt("questionario"));
		sessao.setQuestionarioPrefixo(rs.getString("prefixo"));
		sessao.setEscola(rs.getString("nome"));

		return sessao;
	}

	public void atualizarReferencias(Map<SessaoKey, Sessao> sessoes,
			Map<Integer, Usuario> usuarios,
			Map<Integer, Aluno> alunos,
			Map<Integer, Questionario> questionarios)
	{
		for (Sessao sessao : sessoes.toArray())
		{
			SessaoKey key = sessao.getKey();

			key.setAluno(getAlunoOf(alunos, key.getAluno()));
			key.setQuestionario(getQuestionarioOf(questionarios, key.getQuestionario()));
			key.setUsuario(getUsuarioOf(usuarios, key.getUsuario()));
		}
	}

	private Aluno getAlunoOf(Map<Integer, Aluno> alunos, Aluno aluno)
	{
		for (Aluno a : alunos)
			if (a.equals(aluno))
				return a;

		throw new RuntimeException("Aluno de id " +aluno.getId()+ " n�o encontrado.");
	}

	private Questionario getQuestionarioOf(Map<Integer, Questionario> questionarios, Questionario questionario)
	{
		for (Questionario q : questionarios)
			if (q.getId() == questionario.getId())
				return q;

		throw new RuntimeException("Question�rio de id " +questionario.getId()+ " n�o encontrado.");
	}

	private Usuario getUsuarioOf(Map<Integer, Usuario> usuarios, Usuario usuario)
	{
		for (Usuario u : usuarios)
			if (u.equals(usuario))
				return u;

		throw new RuntimeException("Usu�rio de id " +usuario.getId()+ " n�o encontrado.");
	}
}
