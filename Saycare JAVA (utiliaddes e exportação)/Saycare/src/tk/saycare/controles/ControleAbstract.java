package tk.saycare.controles;

import java.sql.Connection;
import java.sql.Date;
import java.text.ParseException;

import org.diverproject.util.DateUtil;

import tk.saycare.Conexao;

public class ControleAbstract
{
	protected final static int MIN_ALUNO_ID = 1000000;
	protected final static int MAX_ALUNO_ID = 7999999;
	protected final static Date MIN_DATE;
	protected final static Date MAX_DATE;

	static
	{
		MAX_DATE = new Date(System.currentTimeMillis());
		MIN_DATE = new Date(System.currentTimeMillis());

		try {

			java.util.Date minDate = DateUtil.toDate("01/01/2015");
			MIN_DATE.setTime(minDate.getTime());

		} catch (ParseException e) {
		}
	}

	protected Connection connection;

	public ControleAbstract()
	{
		connection = Conexao.getConnection();
	}

}
