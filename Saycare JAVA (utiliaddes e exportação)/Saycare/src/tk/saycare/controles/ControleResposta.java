package tk.saycare.controles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.lang.IntUtil;

import tk.saycare.entidades.Questionario;
import tk.saycare.entidades.Respostas;
import tk.saycare.entidades.Sessao;
import tk.saycare.entidades.SessaoKey;
import tk.saycare.util.MapSessaoAluno;

public class ControleResposta extends ControleAbstract
{
	private static int espaco[][];

	public void calcularEspaco() throws SQLException
	{
		if (espaco != null)
			return;

		calcularQuestionarios();
		calcularPerguntas();
		calcularOrdens();
	}

	private void calcularQuestionarios() throws SQLException
	{
		String sql = "SELECT COUNT(*) as count FROM questionarios";

		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		rs.next();

		espaco = new int[rs.getInt("count")][];
	}

	private void calcularPerguntas() throws SQLException
	{
		String sql = "SELECT MAX(pergunta) as perguntas "
					+"FROM respostas "
					+"WHERE questionario = ? AND (aluno >= ? AND aluno <= ?)";

		for (int i = 0; i < espaco.length; i++)
		{
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, i + 1);
			ps.setInt(2, MIN_ALUNO_ID);
			ps.setInt(3, MAX_ALUNO_ID);

			ResultSet rs = ps.executeQuery();
			rs.next();

			espaco[i] = new int[rs.getInt("perguntas")];
		}
	}

	private void calcularOrdens() throws SQLException
	{
		String sql = "SELECT pergunta, MAX(ordem) as ordens "
					+"FROM respostas "
					+"WHERE questionario = ? AND (aluno >= ? AND aluno <= ?)"
					+"GROUP BY pergunta";

		for (int i = 0; i < espaco.length; i++)
		{
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, i + 1);
			ps.setInt(2, MIN_ALUNO_ID);
			ps.setInt(3, MAX_ALUNO_ID);

			ResultSet rs = ps.executeQuery();

			while (rs.next())
				espaco[i][rs.getInt("pergunta") - 1] = rs.getInt("ordens");
		}
	}

	private Respostas criarMatrizRepostas(Questionario questionario)
	{
		int quantidade = espaco[questionario.getId() - 1].length;

		Respostas respostas = new Respostas();
		respostas.setPerguntas(quantidade);

		for (int pergunta = 1; pergunta <= quantidade; pergunta++)
		{
			int ordens = espaco[questionario.getId() - 1][pergunta - 1];
			respostas.setPerguntaOrdens(pergunta, ordens);
		}

		return respostas;
	}

	public int carregar(Map<SessaoKey, Sessao> sessoes) throws SQLException
	{
		int perguntasCarregadas = 0;

		for (Sessao sessao : sessoes)
			perguntasCarregadas += carregar(sessao);

		return perguntasCarregadas;
	}

	private int carregar(Sessao sessao) throws SQLException
	{
		int perguntasCarregadas = 0;

		String sql = "SELECT pergunta, ordem, resposta FROM respostas "
					+"WHERE aluno = ? AND questionario = ? AND entrada = ? "
					+"ORDER BY pergunta, ordem";

		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, sessao.getKey().getAluno().getId());
		ps.setInt(2, sessao.getKey().getQuestionario().getId());
		ps.setInt(3, sessao.getKey().getEntrada());

		ResultSet rs = ps.executeQuery();
		Respostas respostas = criarMatrizRepostas(sessao.getKey().getQuestionario());

		while (rs.next())
		{
			int pergunta = rs.getInt("pergunta");
			int ordem = rs.getInt("ordem");
			String resposta = rs.getString("resposta");

			respostas.setResposta(pergunta, ordem, resposta);

			perguntasCarregadas++;
		}

		if (perguntasCarregadas > 0)
			sessao.setRespostas(respostas);

		return perguntasCarregadas;
	}

	public MapSessaoAluno mapear(Map<SessaoKey, Sessao> sessoes)
	{
		MapSessaoAluno mapa = new MapSessaoAluno();

		for (Sessao sessao : sessoes)
			mapa.adicionar(sessao);

		return mapa;
	}

	public int getPerguntasCount(int questionario)
	{
		if (IntUtil.interval(questionario, 1, espaco.length))
			return espaco[questionario - 1].length;

		return 0;
	}

	public int getPerguntaOrdens(int pergunta, int questionario)
	{
		if (IntUtil.interval(questionario, 1, espaco.length))
			if (IntUtil.interval(pergunta, 1, espaco[questionario - 1].length))
			return espaco[questionario - 1][pergunta - 1];

		return 0;
	}
}
