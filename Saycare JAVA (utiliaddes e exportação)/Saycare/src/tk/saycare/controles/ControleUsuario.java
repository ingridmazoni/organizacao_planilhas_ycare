package tk.saycare.controles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;

import tk.saycare.entidades.Usuario;
import tk.saycare.util.Sexo;

public class ControleUsuario extends ControleAbstract
{
	public Map<Integer, Usuario> listarTodos() throws SQLException
	{
		String sql = "SELECT * FROM usuarios";

		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		Map<Integer, Usuario> usuarios = new IntegerLittleMap<Usuario>();

		while (rs.next())
		{
			Usuario usuario = criar(rs);
			usuarios.add(usuario.getId(), usuario);
		}

		return usuarios;
	}

	private Usuario criar(ResultSet rs) throws SQLException
	{
		Usuario usuario = new Usuario();
		usuario.setId(rs.getInt("id"));
		usuario.setUsuario(rs.getString("usuario"));
		usuario.setSenha(rs.getString("senha"));
		usuario.setNome(rs.getString("nome"));
		usuario.setUltimoNome(rs.getString("ultimo_nome"));
		usuario.setSexo(Sexo.parse(rs.getString("sexo")));
		usuario.setEmail(rs.getString("email"));
		usuario.setAcesso(rs.getInt("acesso"));
		usuario.setFuncao(rs.getString("funcao"));

		return usuario;
	}
}
