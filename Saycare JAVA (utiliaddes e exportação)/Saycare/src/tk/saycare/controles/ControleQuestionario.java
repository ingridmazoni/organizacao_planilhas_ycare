package tk.saycare.controles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.diverproject.util.collection.Map;
import org.diverproject.util.collection.abstraction.IntegerLittleMap;

import tk.saycare.entidades.Questionario;

public class ControleQuestionario extends ControleAbstract
{
	public Map<Integer, Questionario> listarTodos() throws SQLException
	{
		String sql = "SELECT * FROM questionarios";

		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		Map<Integer, Questionario> questionarios = new IntegerLittleMap<Questionario>();

		while (rs.next())
		{
			Questionario uestionario = criar(rs);
			questionarios.add(uestionario.getId(), uestionario);
		}

		return questionarios;
	}

	private Questionario criar(ResultSet rs) throws SQLException
	{
		Questionario questionario = new Questionario();
		questionario.setId(rs.getInt("id"));
		questionario.setNome(rs.getString("nome"));
		questionario.setPrefixo(rs.getString("prefixo"));

		return questionario;
	}
}
